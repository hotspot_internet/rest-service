= Overview

This document describes the design of the REST API.

== Security

See <<security.adoc#,security document>> for security considerations,
including allowed HTTP protocols and corresponding ports.

== REST API

The following contains an overview of URLs and associated methods:

|===
3+|URL |POST |GET |GET params |Description

|/reading
|/wan-router
|
|X
|X
|See <<reading-params, params>>

|Gets/posts readings for entire WAN router (_e.g._ system CPU usage)

|
|/interface
|/{iface_index}
|X
|X
|See <<reading-params, params>>
|See <<reading, /reading>>


|/activity
|/interface
|/{iface_index}
|
|X
|See <<activity, /activity>>
|See <<activity, /activity>>

|/datalimit
|/interface
|/{iface_index}
|X
|X
|See <<datalimit-params, params>>
|See <<datalimit, /datalimit>>

|===

=== [[reading]] /reading

==== POST

Using the `POST` command, readings are submitted for persistent storage with their
original timestamps.

==== GET

It is of more interest to retrieve readings at specified timestamps rather than at
the timestamps when the readings occurred; therefore, the `GET` call has several
parameters for determining the timepoints at which readings should be returned.
Interpolation of actual readings is used to calculate values for these timepoints of
interest. In summary, a `GET` request to this endpoint returns values interpolated
to match timestamps for specified <<interval-param,interval>> and
<<start-param,start>> params.

==== [[reading-params]] reading parameters

The following params are available for `GET` calls:


|===
|Param |Required |Default |Details

|<<interval-param,interval>>
|NO
|See <<interval-param,details>>
|See <<interval-param,details>>

|quantity
|NO
|1
|Number of readings to return, [1-100]

|<<start-param,start>>
|NO
|See <<start-param,details>>
|See <<start-param,details>>

|===

===== [[interval-param]] interval

If the `interval` parameter is omitted, then a list containing a single
reading will be returned. Interval options:

* MINUTE
* HOUR
* DAY
* MONTH
* YEAR

===== [[start-param]] start

This is a timestamp that indicates the start of the interval-based range described
above. If omitted, then a `start` value will be assumed such that the end of the range
is the most-recent reading recorded for the system.

=== [[activity]] /activity

The activity endpoint allows for retrieval of a list of activities, where each
activity indicates a period (_i.e._ start and stop instant) and the amount of
activity (_e.g._ bytes/packets sent/received) for that period.


|===
|Param |Required |Default |Details

|period
|NO
|MONTH
|This parameter allows the same values as the <<interval-param,interval>>
parameter of the '/reading/...' endpoint.

|quantity
|NO
|1
|Number of activities to return (if `divided=false`), otherwise see
<<divided-param,details>>.

|<<start-param,start>>
|NO
|See <<start-param,details>>
|See <<start-param,details>>

|divided
|NO
|`false`
|boolean (_i.e._ 'true' or 'false'). See <<divided-param,details>> 

|===

==== [[divided-param]] divided param

Specifying `true` results in the activities getting divided into shorter-duration
periods spanning the same total duration. For example, if the request URL is the
following:

    /activity/interface/1?start=2021-01-20T00:00:00Z&period=DAY&quantity=1&divided=true

Then for the single day of activity requested, a list of 24 1-hour activities is
returned. If a single hour was requested instead, then 60 1-minute activities is
returned. If the requested period does not have a shorter-duration period (as is the
case for `period=MINUTE`), then a 404 error would be returned. Yes, a minute can be
further broken into seconds; however, currently `period=SECOND` is not supported for
this API.

If, for the above example URL, `quantity=2` was requested, then 48 1-hour activities would
be returned.

=== [[datalimit]] /datalimit

Gets/posts datalimit (_i.e._ how many GB of data per month or day) for each interface.

==== [[datalimit-params]] datalimit parameters

* `all`=[true,false]
** If set to `true` returns all data limits, including inactive ones
** default is `false`
* instant=<timestamp>
** indicates the instant for which data limits are needed
** default is <Instant of now>
** Ignored if `all`=true

== Hypermedia

Hypermedia links within HTTP responses allow for clients to navigate the API with
minimal knowledge of how the API works. For the '/activity/interface/<index>' URL,
responses to `GET` requests represent a collection with the following general format:

```
{
    _embedded: {
        interfaceActivityList: [
            {
                start: <start>
                stop: <stop>
                bytesIn: <bytes in>
                ... other activity fields
                _links: <links object>
            },
            ... other activities
        ]
    }
    _links: <links object>
}

```

The `_links` object has several keys with each key corresponding to a link.
The value for each key is an object containing an HREF:

```
_links {
    <link1>: {
        href: <href value>
    }
    ... other links
}
```

The following links (See https://www.iana.org/assignments/link-relations/link-relations.xhtml[IANA link definitions])
are available:

* prev
* self
* next
* intervalContains
* intervalIn

`intervalContains` allows for requesting a breakdown of the activity into
shorter duration periods spanning the same total period
(see <<divided-param,divided param>>).

`intervalIn` allows for requesting the 'level-up' activity. As an example,
if the given activity is an hour of activity starting at 11:00 on a day,
then `intervalIn` will refer to a day of activity starting at 00:00 on that
same day. Other period units will result in the following behavior for
`intervalIn`:

* MINUTE -> HOUR of activity starting at top of that same hour
* HOUR -> DAY of activity starting at 0:00 AM of that same day
* DAY -> MONTH of activity
** Beginning at the start of the data limit cycle in which the day occurs
** For example, if cycle starts on 19th of each month, then `intervalIn` for
February 3rd will be a month of activity starting on January 19.
* MONTH -> YEAR of activity starting at previous January cycle start
* YEAR -> `intervalIn` N/A

The JSON above is the same for '/reading/interface/{index}', with the following
differences:

* Because a reading does not represent an interval:
** no `intervalContains` link
** no `intervalIn` link
* `interfaceReadingList` instead of `interfaceActivityList`
