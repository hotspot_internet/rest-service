= Overview

This document describes the notifications API.

== Notifications API

|===
2+|URL |POST |GET |GET params |Description

|/notifications
|/overage
|
|X
|See <<overage, details>>
|See <<overage, details>>

|===

== [[overage]] /notifications/overage

`GET` requests to the 'notifications/overage' URL receive a response
containing overage notifications. There are two types of notifications:

* <<overage-cycle, cycle-based>>
** maximum of 1 notification/day
** each overage stored as row in database
* <<overage-hour, hour-based>>
** maximum of 1 notification/hour
** each overage stored as row in database

See following table for parameters:

|===

|Param |Required |Default |Details

|start
|See below^1^
|NONE
|Start of period for which to get notifications.

|stop
|See below^1^
|NONE
|End of period for which to get notifications.

|===

^1^`start`/`stop` to be specified if and only if the other is specified.

Upon receiving a `GET` request:

* if `start`/`stop` absent: calculate whether there is currently an
overage that has not been previously calculated
* Else: return all notifications within specified start/stop range

=== [[overage-cycle]] Cycle-based Notifications

* Used (USE): GB usage from start of cycle until 'now'
* On-pace Usage (OPU): GB that should be used at end of day in order
to maintain a pace that uses exactly the allowed amount for current cycle
* Usage overage = USE - OPU
* If usage overage > 6 GB -> generate notification

=== [[overage-hour]] Hour-based notifications

* Calculate usage overage (for entire cycle up until 'now')
* Calculate usage from start of hour until 'now'
* Generate notification if both true:
** Usage overage > 6 GB
** hour usage exceeds 1% of allowed cycle usage
