# Overview

This project stores code for the REST API and implementation of
processing those calls to the REST API. It also contains code for the runner
of the REST service.

## Details

See design documents in artifacts of this project for additional details.

## Running the Application

Using the executable within build/distributions/rest-service-boot (created from
`./gradlew build` command), the application can be run in the following manner:

    ./rest-service --<spring-property-name>=<property-value>

The following outlines spring properties that may be of particular interest
to override/set:

* General:
    * spring.resources.static-locations=<path to static files>
        * For example, REST API design documents
* Datasource:
    * spring.datasource.username
    * spring.datasource.password
    * spring.datasource.url
* Security:
    * server.ssl.key-store=/path/to/keystore.jks
    * server.ssl.key-store-password=<keystore password>
    * server.ssl.key-alias=<server IP>
    * server.ssl.key-password=<key password>
    * server.ssl.trust-store=/path/to/trustore.jks
    * server.ssl.trust-store-password=<truststore password>

The values used for the datasource properties depend on how the database
was created and where it resides.

See _docs/security.adoc_ for examples of how to create the key and trust
stores referenced by the configuration settings above. Additionally, that
document indicates passwords and other information (_e.g._ <server IP>)
necessary for specifying the security configuration.

NOTE: The Security settings do not have defaults and therefore must be
configured in order for the application to run.
