package hotspotinternet.restservice.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;

import com.google.common.testing.EqualsTester;

import hotspotinternet.restservice.entity.DataLimit.CyclePeriod;

public class DataLimitTest {
    private static final Instant NOW = Instant.now();
    private static final ZonedDateTime EPOCH_ADJUSTED = Instant.EPOCH
            .atZone(ZoneOffset.UTC)
            .withDayOfMonth(4);

    @Test
    public void testIsForRecurringMonthly() throws Exception {
        DataLimit dataLimit = DataLimit.recurring(
                1,
                50L,
                NOW.minusMillis(100),
                CyclePeriod.MONTH);
        assertTrue(dataLimit.isFor(NOW));
        assertFalse(dataLimit.isFor(NOW.minusMillis(200)));
    }

    @Test
    public void testIsForRecurringWithStop() throws Exception {
        DataLimit dataLimit = DataLimit.recurringWithStop(
                1,
                50L,
                NOW.minusMillis(100),
                NOW.plusMillis(100),
                CyclePeriod.MONTH);
        assertTrue(dataLimit.isFor(NOW));
        assertFalse(dataLimit.isFor(NOW.plusMillis(200)));
    }

    @Test
    public void testIsForOneShot() throws Exception {
        DataLimit dataLimit = DataLimit.oneShot(
                1,
                50L,
                NOW.minusMillis(100),
                NOW.plusMillis(100));
        assertTrue(dataLimit.isFor(NOW));
        assertFalse(dataLimit.isFor(NOW.minusMillis(200)));
    }

    @Test
    public void testGetCycleStartMonthly() throws Exception {
        Instant novemberEpoch = EPOCH_ADJUSTED
                .withMonth(11)
                .toInstant();
        Instant decemberEpoch = EPOCH_ADJUSTED
                .withMonth(12)
                .toInstant();
        DataLimit dataLimit = DataLimit.recurring(
                1,
                50L,
                novemberEpoch,
                CyclePeriod.MONTH);
        Instant january = EPOCH_ADJUSTED
                .withMonth(1)
                .withYear(EPOCH_ADJUSTED.getYear() + 1)
                .toInstant()
                .minusMillis(500);
        assertEquals(decemberEpoch, dataLimit.getStartInstant(january).get());
    }

    @Test
    public void testGetCycleStartOneShot() throws Exception {
        Instant novemberEpoch = EPOCH_ADJUSTED
                .withMonth(11)
                .toInstant();
        Instant decemberEpoch = EPOCH_ADJUSTED
                .withMonth(12)
                .toInstant();
        DataLimit dataLimit = DataLimit.oneShot(1, 50L, novemberEpoch, decemberEpoch);
        assertEquals(
                novemberEpoch,
                dataLimit.getStartInstant(decemberEpoch.minusMillis(1)).get());
    }

    @Test
    public void testGetCycleStartAbsent() throws Exception {
        Instant novemberEpoch = EPOCH_ADJUSTED
                .withMonth(11)
                .toInstant();
        Instant decemberEpoch = EPOCH_ADJUSTED
                .withMonth(12)
                .toInstant();
        DataLimit dataLimit = DataLimit.recurring(
                1,
                50L,
                decemberEpoch,
                CyclePeriod.MONTH);
        assertFalse(dataLimit.getStartInstant(novemberEpoch).isPresent());
    }

    @Test
    public void testGetCycleStopMonthly() throws Exception {
        Instant november = EPOCH_ADJUSTED
                .withMonth(11)
                .toInstant();
        Instant nextJanuary = EPOCH_ADJUSTED
                .withMonth(1)
                .withYear(EPOCH_ADJUSTED.getYear() + 1)
                .toInstant();
        DataLimit dataLimit = DataLimit.recurringWithStop(
                1,
                50L,
                november,
                nextJanuary,
                CyclePeriod.MONTH);
        assertEquals(nextJanuary, dataLimit.getStopInstant(nextJanuary.minusMillis(500)).get());
        assertFalse(dataLimit.getStopInstant(nextJanuary.plusMillis(500)).isPresent());
    }

    @Test
    public void testGetCycleStopBeforeCycleEnd() throws Exception {
        ZonedDateTime february = Instant.EPOCH
                .atZone(ZoneOffset.UTC)
                .withMonth(2);
        ZonedDateTime february15th = february
                .withDayOfMonth(15);
        DataLimit dataLimit = DataLimit.recurringWithStop(
                1,
                50L,
                Instant.EPOCH,
                february15th.toInstant(),
                CyclePeriod.MONTH);
        assertEquals(
                february15th.toInstant(),
                dataLimit.getStopInstant(february.toInstant().plusMillis(1)).get());
    }

    @Test
    public void testEquals() throws Exception {
        DataLimit dataLimitA1 = DataLimit.recurring(
                1,
                50L,
                NOW.minusMillis(100),
                CyclePeriod.MONTH);
        DataLimit dataLimitA2 = DataLimit.recurring(
                1,
                50L,
                NOW.minusMillis(100),
                CyclePeriod.MONTH);
        DataLimit dataLimitB = DataLimit.recurringWithStop(
                1,
                50L,
                NOW.minusMillis(100),
                NOW.plusMillis(100),
                CyclePeriod.MONTH);
        DataLimit dataLimitC1 = DataLimit.oneShot(
                1,
                50L,
                NOW.minusMillis(100),
                NOW.plusMillis(100));
        DataLimit dataLimitC2 = DataLimit.oneShot(
                1,
                50L,
                NOW.minusMillis(100),
                NOW.plusMillis(100));
        new EqualsTester()
                .addEqualityGroup(dataLimitA1, dataLimitA2)
                .addEqualityGroup(dataLimitB)
                .addEqualityGroup(dataLimitC1, dataLimitC2)
                .testEquals();
    }
}
