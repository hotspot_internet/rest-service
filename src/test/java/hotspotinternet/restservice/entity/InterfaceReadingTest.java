package hotspotinternet.restservice.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Instant;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class InterfaceReadingTest {

    @Test
    public void testZero() throws Exception {
        InterfaceReading interfaceReading = new InterfaceReading(1L,
                1,
                Instant.EPOCH,
                AdminStatus.DOWN,
                OperStatus.UP,
                1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L, 11L);
        assertEquals(
                new InterfaceReading(1L,
                        1,
                        Instant.EPOCH,
                        AdminStatus.DOWN,
                        OperStatus.UP,
                        0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L),
                interfaceReading.zero());
    }

    @Test
    public void testAdd() throws Exception {
        InterfaceReading interfaceReading = new InterfaceReading(1L,
                1,
                Instant.EPOCH,
                AdminStatus.DOWN,
                OperStatus.UP,
                1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L, 11L);
        assertEquals(
                new InterfaceReading(1L,
                        1,
                        Instant.EPOCH,
                        AdminStatus.DOWN,
                        OperStatus.UP,
                        2L, 4L, 6L, 8L, 10L, 12L, 14L, 16L, 18L, 20L, 22L),
                interfaceReading.add(interfaceReading));
    }

    @Test
    public void testMinus() throws Exception {
        InterfaceReading readingA = new InterfaceReading(1L,
                1,
                Instant.EPOCH,
                AdminStatus.DOWN,
                OperStatus.UP,
                11L, 22L, 33L, 44L, 55L, 66L, 77L, 88L, 99L, 110L, 121L);
        InterfaceReading readingB = new InterfaceReading(1L,
                1,
                Instant.EPOCH,
                AdminStatus.UP,
                OperStatus.DOWN,
                1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L, 11L);
        InterfaceReading readingC = new InterfaceReading(1L,
                1,
                Instant.EPOCH,
                AdminStatus.DOWN,
                OperStatus.UP,
                10L, 20L, 30L, 40L, 50L, 60L, 70L, 80L, 90L, 100L, 110L);
        assertEquals(
                readingC,
                readingA.substract(readingB));
    }

    @Test
    public void testGetEstimatedReading() throws Exception {
        InterfaceReading readingA = new InterfaceReading(1L,
                1,
                Instant.EPOCH,
                AdminStatus.DOWN,
                OperStatus.UP,
                10L, 20L, 30L, 40L, 50L, 60L, 70L, 80L, 90L, 100L, 110L);
        InterfaceReading readingB = new InterfaceReading(1L,
                1,
                Instant.EPOCH.plusMillis(200),
                AdminStatus.UP,
                OperStatus.DOWN,
                30L, 40L, 50L, 60L, 70L, 80L, 90L, 100L, 110L, 120L, 130L);
        InterfaceReading expectedInterpolation = new InterfaceReading(null,
                1,
                Instant.EPOCH.plusMillis(50),
                AdminStatus.DOWN,
                OperStatus.UP,
                15L, 25L, 35L, 45L, 55L, 65L, 75L, 85L, 95L, 105L, 115L);
        InterfaceReading actualInterpolation = InterfaceReading.getEstimatedReading(
                readingA,
                readingB,
                Instant.EPOCH.plusMillis(50));
        assertEquals(
                expectedInterpolation,
                actualInterpolation);
    }

    @Test
    public void testGetEstimatedAfterReadings() throws Exception {
        InterfaceReading readingA = new InterfaceReading(1L,
                1,
                Instant.EPOCH,
                AdminStatus.DOWN,
                OperStatus.UP,
                10L, 20L, 30L, 40L, 50L, 60L, 70L, 80L, 90L, 100L, 110L);
        InterfaceReading readingB = new InterfaceReading(1L,
                1,
                Instant.EPOCH.plusMillis(200),
                AdminStatus.UP,
                OperStatus.DOWN,
                30L, 40L, 50L, 60L, 70L, 80L, 90L, 100L, 110L, 120L, 130L);
        InterfaceReading expectedInterpolation = new InterfaceReading(null,
                1,
                Instant.EPOCH.plusMillis(300),
                AdminStatus.UP,
                OperStatus.DOWN,
                40L, 50L, 60L, 70L, 80L, 90L, 100L, 110L, 120L, 130L, 140L);
        InterfaceReading actualInterpolation = InterfaceReading.getEstimatedReading(
                readingA,
                readingB,
                Instant.EPOCH.plusMillis(300));
        assertEquals(
                expectedInterpolation,
                actualInterpolation);
    }

    @Test
    public void testGetEstimatedSameReading() throws Exception {
        InterfaceReading readingA = new InterfaceReading(1L,
                1,
                Instant.EPOCH,
                AdminStatus.DOWN,
                OperStatus.UP,
                10L, 20L, 30L, 40L, 50L, 60L, 70L, 80L, 90L, 100L, 110L);
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> InterfaceReading.getEstimatedReading(readingA, readingA, Instant.EPOCH));
    }
}
