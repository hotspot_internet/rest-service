package hotspotinternet.restservice.interval;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Instant;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.google.common.base.Optional;
import com.google.common.testing.EqualsTester;

import hotspotinternet.restservice.entity.DataLimit;
import hotspotinternet.restservice.entity.DataLimit.CyclePeriod;

public class ActivityPeriodsFactoryTest {

    private static ActivityPeriodsFactory createDut(final Instant dataLimitStart) {
        return new ActivityPeriodsFactory(
                DataLimit.recurring(1, 0L, dataLimitStart, CyclePeriod.MONTH));
    }

    @Test
    public void testYearPeriods() throws Exception {
        Instant dataLimitStart = Instant.parse("1970-01-01T00:00:00Z");
        ActivityPeriodsFactory dut = createDut(dataLimitStart);

        ActivityPeriods yearPeriods = dut.yearPeriods(dataLimitStart);
        assertEquals(ReadingInterval.YEAR, yearPeriods.getDuration());
        assertEquals(Optional.absent(), yearPeriods.getUpPeriods());

        List<ActivityPeriods> downPeriods = yearPeriods.getDownPeriods().get();
        assertEquals(12, downPeriods.size());
        assertEquals(ReadingInterval.MONTH, downPeriods.get(0).getDuration());
        assertEquals(dataLimitStart, downPeriods.get(0).getStart());
        assertEquals(Instant.parse("1970-12-01T00:00:00Z"), downPeriods.get(11).getStart());
    }

    @Test
    public void testCyclePeriodsUpIsPreviousYear() throws Exception {
        Instant dataLimitStart = Instant.parse("1970-01-19T00:00:00Z");
        ActivityPeriodsFactory dut = createDut(dataLimitStart);

        ActivityPeriods cyclePeriods = dut.cyclePeriods(
                Instant.parse("1971-01-18T00:00:00Z"));

        assertEquals(ReadingInterval.MONTH, cyclePeriods.getDuration());

        ActivityPeriods upPeriods = cyclePeriods.getUpPeriods().get();
        assertEquals(ReadingInterval.YEAR, upPeriods.getDuration());
        assertEquals(Instant.parse("1970-01-19T00:00:00Z"), upPeriods.getStart());

        List<ActivityPeriods> downPeriods = cyclePeriods.getDownPeriods().get();
        assertEquals(31, downPeriods.size());
        assertEquals(ReadingInterval.DAY, downPeriods.get(0).getDuration());
        assertEquals(Instant.parse("1971-01-18T00:00:00Z"), downPeriods.get(0).getStart());
        assertEquals(Instant.parse("1971-02-17T00:00:00Z"), downPeriods.get(30).getStart());
    }

    @Test
    public void testCyclePeriodsUpIsSameYear() throws Exception {
        Instant dataLimitStart = Instant.parse("1970-01-19T00:00:00Z");
        ActivityPeriodsFactory dut = createDut(dataLimitStart);
        ActivityPeriods cyclePeriods = dut.cyclePeriods(
                Instant.parse("1971-01-20T00:00:00Z"));
        ActivityPeriods upPeriods = cyclePeriods.getUpPeriods().get();
        assertEquals(Instant.parse("1971-01-19T00:00:00Z"), upPeriods.getStart());
    }

    @Test
    public void testDayPeriodsUpIsPreviousMonth() throws Exception {
        Instant dataLimitStart = Instant.parse("1970-01-19T00:00:00Z");
        ActivityPeriodsFactory dut = createDut(dataLimitStart);

        ActivityPeriods dayPeriods = dut.dayPeriods(
                Instant.parse("1971-01-01T01:00:00Z"));

        assertEquals(ReadingInterval.DAY, dayPeriods.getDuration());

        ActivityPeriods upPeriods = dayPeriods.getUpPeriods().get();
        assertEquals(ReadingInterval.MONTH, upPeriods.getDuration());
        assertEquals(Instant.parse("1970-12-19T00:00:00Z"), upPeriods.getStart());

        List<ActivityPeriods> downPeriods = dayPeriods.getDownPeriods().get();
        assertEquals(24, downPeriods.size());
        assertEquals(ReadingInterval.HOUR, downPeriods.get(0).getDuration());
        assertEquals(Instant.parse("1971-01-01T01:00:00Z"), downPeriods.get(0).getStart());
        assertEquals(Instant.parse("1971-01-02T00:00:00Z"), downPeriods.get(23).getStart());
    }

    @Test
    public void testDayPeriodsUpIsSameMonth() throws Exception {
        Instant dataLimitStart = Instant.parse("1970-01-19T00:00:00Z");
        ActivityPeriodsFactory dut = createDut(dataLimitStart);

        ActivityPeriods dayPeriods = dut.dayPeriods(
                Instant.parse("1971-01-20T01:00:00Z"));

        ActivityPeriods upPeriods = dayPeriods.getUpPeriods().get();
        assertEquals(ReadingInterval.MONTH, upPeriods.getDuration());
        assertEquals(Instant.parse("1971-01-19T00:00:00Z"), upPeriods.getStart());
    }

    @Test
    public void testHourPeriods() throws Exception {
        Instant dataLimitStart = Instant.parse("1970-01-01T00:00:00Z");
        ActivityPeriodsFactory dut = createDut(dataLimitStart);

        ActivityPeriods hourPeriods = dut.hourPeriods(
                Instant.parse("1970-01-01T01:10:00Z"));

        assertEquals(ReadingInterval.HOUR, hourPeriods.getDuration());

        ActivityPeriods upPeriods = hourPeriods.getUpPeriods().get();
        assertEquals(ReadingInterval.DAY, upPeriods.getDuration());
        assertEquals(Instant.parse("1970-01-01T00:00:00Z"), upPeriods.getStart());

        List<ActivityPeriods> downPeriods = hourPeriods.getDownPeriods().get();
        assertEquals(60, downPeriods.size());
        assertEquals(ReadingInterval.MINUTE, downPeriods.get(0).getDuration());
        assertEquals(Instant.parse("1970-01-01T01:10:00Z"), downPeriods.get(0).getStart());
        assertEquals(Instant.parse("1970-01-01T02:09:00Z"), downPeriods.get(59).getStart());
    }

    @Test
    public void testHourPeriodsPreviousDay() throws Exception {
        Instant dataLimitStart = Instant.parse("1969-01-01T05:00:00Z");
        ActivityPeriodsFactory dut = createDut(dataLimitStart);

        ActivityPeriods hourPeriods = dut.hourPeriods(
                Instant.parse("1970-01-01T03:10:00Z"));

        ActivityPeriods upPeriods = hourPeriods.getUpPeriods().get();
        assertEquals(ReadingInterval.DAY, upPeriods.getDuration());
        assertEquals(Instant.parse("1969-12-31T05:00:00Z"), upPeriods.getStart());
    }

    @Test
    public void testMinutePeriods() throws Exception {
        Instant dataLimitStart = Instant.parse("1970-01-01T00:00:00Z");
        ActivityPeriodsFactory dut = createDut(dataLimitStart);

        ActivityPeriods minutePeriods = dut.minutePeriods(
                Instant.parse("1970-01-01T00:01:05Z"));

        assertEquals(ReadingInterval.MINUTE, minutePeriods.getDuration());

        ActivityPeriods upPeriods = minutePeriods.getUpPeriods().get();
        assertEquals(ReadingInterval.HOUR, upPeriods.getDuration());
        assertEquals(Instant.parse("1970-01-01T00:01:00Z"), upPeriods.getStart());

        assertEquals(Optional.absent(), minutePeriods.getDownPeriods());
    }

    @Test
    public void testEquals() {
        Instant dataLimitStart = Instant.parse("1970-01-01T00:00:00Z");
        ActivityPeriodsFactory dut = createDut(dataLimitStart);

        Instant instantA = Instant.parse("1970-01-01T00:00:00Z");
        Instant instantB = Instant.parse("1970-01-01T00:00:01Z");
        ActivityPeriods minutePeriodsA1 = dut.minutePeriods(instantA);
        ActivityPeriods minutePeriodsA2 = dut.minutePeriods(instantA);
        ActivityPeriods minutePeriodsB1 = dut.minutePeriods(instantB);
        ActivityPeriods hourPeriodsA1 = dut.hourPeriods(instantA);
        ActivityPeriods hourPeriodsA2 = dut.hourPeriods(instantA);
        new EqualsTester()
                .addEqualityGroup(minutePeriodsA1, minutePeriodsA2)
                .addEqualityGroup(minutePeriodsB1)
                .addEqualityGroup(hourPeriodsA1, hourPeriodsA2)
                .testEquals();
    }
}
