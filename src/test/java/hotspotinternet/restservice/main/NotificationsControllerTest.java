package hotspotinternet.restservice.main;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.client.RestTemplate;

import hotspotinternet.restservice.entity.DataLimit;
import hotspotinternet.restservice.entity.DataLimit.CyclePeriod;
import hotspotinternet.restservice.entity.DataLimitRepository;
import hotspotinternet.restservice.entity.InterfaceActivity;
import hotspotinternet.restservice.entity.NetworkActivity;
import hotspotinternet.restservice.entity.Overage;
import hotspotinternet.restservice.entity.Overage.OverageType;
import hotspotinternet.restservice.entity.OverageRepository;
import hotspotinternet.restservice.entity.UsageReport;
import hotspotinternet.restservice.entity.UsageReportRepository;

@SpringBootTest
@AutoConfigureMockMvc
public class NotificationsControllerTest {
    private static final String MESSAGE_ID = "some ID";
    private static final String SOURCE = "source";
    private static final Instant INSTANT = Instant.now();
    private static final Long USED_BYTES = null;
    private static final Double USED_PERCENT = 90.0;
    private static final String PHONE_NUMBER = "0529";

    @Autowired
    private MockMvc mockMvc;

    private MockRestServiceServer mockServer;

    @MockBean
    private DataLimitRepository mockDataLimitRepo;
    @MockBean
    private OverageRepository mockOverageRepo;
    @MockBean
    private UsageReportRepository mockUsageReportRepo;
    @MockBean
    private RestTemplate restTemplate;
    @MockBean
    private InstantService instantService;

    private static JSONObject createUsageReportJson(final Double usedPercent) {
        return new JSONObject()
                .put("usageReportId", JSONObject.NULL)
                .put("messageId", MESSAGE_ID)
                .put("phoneNumber", PHONE_NUMBER)
                .put("source", SOURCE)
                .put("instant", INSTANT)
                .put("usedBytes", USED_BYTES)
                .put("usedPercent", usedPercent);
    }

    private static JSONObject createUsageReportJson() {
        return createUsageReportJson(USED_PERCENT);
    }

    private static JSONArray createInterfaceUsageJson(
            final int interfaceIndex,
            final Instant instant,
            final long usedBytes,
            final Double usedPercent) {
        return new JSONArray()
                .put(createUsageReportJson(usedPercent)
                        .put("instant", instant)
                        .put("usedBytes", usedBytes)
                        .put("interfaceIndex", interfaceIndex));
    }

    private static JSONObject createOverageJsonObject(
            final String description,
            final OverageType overageType,
            final Instant instant,
            final Long overageId) {
        return new JSONObject()
                .put("description", description)
                .put("overageType", overageType)
                .put("instant", instant)
                .put("overageId", overageId);
    }

    @BeforeEach
    public void setup() {
        mockServer = MockRestServiceServer.bindTo(restTemplate).build();
        setupOverageRepoSave();
    }

    private static ActivityCollectionModel mockActivityCollectionModel(
            final InterfaceActivity interfaceActivity) {
        ActivityCollectionModel activityCollectionModel = mock(ActivityCollectionModel.class);
        when(activityCollectionModel.getContent())
                .thenReturn(Arrays.asList(EntityModel.of(interfaceActivity)));
        return activityCollectionModel;
    }

    @SuppressWarnings("unchecked")
    private void setupActivityResponses(
            Instant cycleStart,
            InterfaceActivity cycleActivity,
            Instant hourStart,
            InterfaceActivity hourActivity) {
        ActivityCollectionModel cycleCollectionModel = mockActivityCollectionModel(cycleActivity);
        ActivityCollectionModel hourCollectionModel = mockActivityCollectionModel(hourActivity);
        when(restTemplate
                .getForObject(contains(String.format(
                        "http://0.0.0.0:8080/activity/interface/1?start=%s&period=MONTH",
                        cycleStart)),
                        any(Class.class)))
                .thenReturn(cycleCollectionModel);
        when(restTemplate
                .getForObject(contains(String.format(
                        "http://0.0.0.0:8080/activity/interface/1?start=%s&period=HOUR",
                        hourStart)),
                        any(Class.class)))
                .thenReturn(hourCollectionModel);
    }

    private void setupInstantSupplier(Instant instant) {
        when(instantService.now()).thenReturn(instant);
    }

    private ResultActions sendOverageGet() throws Exception {
        return sendOverageGet(null, null);
    }

    private ResultActions sendOverageGet(Instant start, Instant stop) throws Exception {
        String getUrl = "/notifications/overage";
        if (start != null || stop != null) {
            getUrl = String.format("%s?start=%s&stop=%s", getUrl, start, stop);
        }
        final MockHttpServletRequestBuilder mockRequestBuilder = get(getUrl);
        mockRequestBuilder.contentType(MediaType.APPLICATION_JSON);
        return mockMvc.perform(mockRequestBuilder);
    }

    private void setupDataLimits(final long bytes, final Instant start) {
        Arrays.asList(1, 2, 3)
                .stream()
                .forEach(interfaceIndex -> {
                    DataLimit dataLimit = DataLimit.recurring(
                            interfaceIndex,
                            bytes,
                            start,
                            CyclePeriod.MONTH);
                    when(mockDataLimitRepo.findByInterfaceIndex(interfaceIndex))
                            .thenReturn(Arrays.asList(dataLimit));
                });
    }

    private void setupOverageRepoSave() {
        when(mockOverageRepo.save(any(Overage.class))).thenAnswer(new Answer<Overage>() {
            @Override
            public Overage answer(InvocationOnMock invocation) throws Throwable {
                Object[] args = invocation.getArguments();
                return (Overage) args[0];
            }
        });
    }

    private static NetworkActivity mockNetworkActivity(long inBytes, long outBytes) {
        NetworkActivity mockCycleActivity = mock(NetworkActivity.class);
        when(mockCycleActivity.getInBytes()).thenReturn(inBytes);
        when(mockCycleActivity.getOutBytes()).thenReturn(outBytes);
        return mockCycleActivity;
    }

    @Test
    public void testPost() throws Exception {
        // given
        UsageReport usageReport = new UsageReport(
                null, MESSAGE_ID, SOURCE,
                INSTANT, null, PHONE_NUMBER, USED_PERCENT);
        when(mockUsageReportRepo.save(any())).thenReturn(usageReport);
        when(mockUsageReportRepo.findFirstByMessageId(MESSAGE_ID))
                .thenReturn(Optional.empty());

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder = post("/notifications/usagereport")
                .with(TestUtil.authenticated());
        mockRequestBuilder.content(createUsageReportJson().toString());
        mockRequestBuilder.contentType(MediaType.APPLICATION_JSON);
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(createUsageReportJson().toString()));
        verify(mockUsageReportRepo).save(usageReport);
    }

    @Test
    public void testPostExistingReport() throws Exception {
        // given
        JSONObject postJson = createUsageReportJson();
        UsageReport usageReport = new UsageReport(
                null, MESSAGE_ID, SOURCE,
                INSTANT, null, PHONE_NUMBER, USED_PERCENT);
        when(mockUsageReportRepo.findFirstByMessageId(MESSAGE_ID))
                .thenReturn(Optional.of(usageReport));

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder = post("/notifications/usagereport")
                .with(TestUtil.authenticated());
        mockRequestBuilder.content(postJson.toString());
        mockRequestBuilder.contentType(MediaType.APPLICATION_JSON);
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isAlreadyReported())
                .andExpect(content().json(createUsageReportJson().toString()));
        verify(mockUsageReportRepo, never()).save(any());
    }

    @Test
    public void testPostNoUsedInformation() throws Exception {
        // given
        JSONObject postJson = createUsageReportJson(null);

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder = post("/notifications/usagereport")
                .with(TestUtil.authenticated());
        mockRequestBuilder.content(postJson.toString());
        mockRequestBuilder.contentType(MediaType.APPLICATION_JSON);
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString(
                        "1 and only 1 of usedBytes and usedPercent should be null")));
        verifyNoInteractions(mockUsageReportRepo);
    }

    @Test
    public void testGet() throws Exception {
        // given
        Instant usageReportInsant = Instant.now();
        Instant dataLimitStart = usageReportInsant.minusSeconds(60);
        UsageReport usageReport = new UsageReport(
                null, MESSAGE_ID, SOURCE,
                usageReportInsant, null, "0529", 100.0);
        UsageReport oldUsageReport = new UsageReport(
                null, MESSAGE_ID, SOURCE,
                Instant.EPOCH, null, "0529", 90.0);
        when(mockUsageReportRepo.findByPhoneNumberContaining("0529"))
                .thenReturn(Arrays.asList(oldUsageReport, usageReport));
        when(mockDataLimitRepo.findByInterfaceIndex(1))
                .thenReturn(Arrays.asList(
                        DataLimit.recurring(1, (long) 30e9, dataLimitStart, CyclePeriod.MONTH)));

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder =
                get("/notifications/usagereport/interface/1");
        mockRequestBuilder.contentType(MediaType.APPLICATION_JSON);
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(TestUtil.jsonMatcher(
                        createInterfaceUsageJson(1, usageReportInsant, (long) 30e9, 100.0))));
    }

    @Test
    public void testGetUsageReportWithBytes() throws Exception {
        // given
        Instant dataLimitStart = Instant.now().minusSeconds(60);
        UsageReport usageReport = new UsageReport(
                null, MESSAGE_ID, SOURCE,
                INSTANT, (long) 15e9, "0529", null);
        when(mockUsageReportRepo.findByPhoneNumberContaining("0529"))
                .thenReturn(Arrays.asList(usageReport));
        when(mockDataLimitRepo.findByInterfaceIndex(1))
                .thenReturn(Arrays.asList(
                        DataLimit.recurring(1, (long) 30e9, dataLimitStart, CyclePeriod.MONTH)));

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder =
                get("/notifications/usagereport/interface/1");
        mockRequestBuilder.contentType(MediaType.APPLICATION_JSON);
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(TestUtil.jsonMatcher(
                        createInterfaceUsageJson(1, INSTANT, (long) 15e9, 50.0))));
    }

    @Test
    public void testGetInPast() throws Exception {
        // given
        Instant oldUsageReportInstant = Instant.EPOCH.plusSeconds(200);
        DataLimit oldDataLimit = DataLimit.recurringWithStop(
                1,
                (long) 30e9,
                Instant.EPOCH,
                Instant.EPOCH.plusSeconds(600),
                CyclePeriod.MONTH);
        DataLimit dataLimit = DataLimit.recurring(
                1,
                (long) 15e9,
                Instant.now().minusSeconds(60),
                CyclePeriod.MONTH);
        UsageReport oldUsageReport = new UsageReport(
                null, MESSAGE_ID, SOURCE,
                oldUsageReportInstant, (long) 15e9, "0529", null);
        UsageReport usageReport = new UsageReport(
                null, MESSAGE_ID, SOURCE,
                Instant.now(), (long) 15e9, "0529", null);
        when(mockUsageReportRepo.findByPhoneNumberContaining("0529"))
                .thenReturn(Arrays.asList(usageReport, oldUsageReport));
        when(mockDataLimitRepo.findByInterfaceIndex(1))
                .thenReturn(Arrays.asList(dataLimit, oldDataLimit));

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder = get(String.format(
                "/notifications/usagereport/interface/1?instant=%s",
                Instant.EPOCH.plusSeconds(300)));
        mockRequestBuilder.contentType(MediaType.APPLICATION_JSON);
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(TestUtil.jsonMatcher(
                        createInterfaceUsageJson(1, oldUsageReportInstant, (long) 15e9, 50.0))));
    }

    @Test
    public void testNoDataLimits() throws Exception {
        // given
        UsageReport usageReport = new UsageReport(
                null, MESSAGE_ID, SOURCE,
                INSTANT, (long) 15e9, "0529", null);
        when(mockUsageReportRepo.findByPhoneNumberContaining("0529"))
                .thenReturn(Arrays.asList(usageReport));
        when(mockDataLimitRepo.findByInterfaceIndex(1)).thenReturn(Collections.emptyList());

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder =
                get("/notifications/usagereport/interface/1");
        mockRequestBuilder.contentType(MediaType.APPLICATION_JSON);
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString("No data limits available")));
    }

    @Test
    public void testNoPhoneNumberForInterface() throws Exception {
        // given
        DataLimit dataLimit = DataLimit.recurring(
                4,
                (long) 15e9,
                Instant.now().minusSeconds(60),
                CyclePeriod.MONTH);
        when(mockDataLimitRepo.findByInterfaceIndex(4))
                .thenReturn(Arrays.asList(dataLimit));

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder =
                get("/notifications/usagereport/interface/4");
        mockRequestBuilder.contentType(MediaType.APPLICATION_JSON);
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString(
                        "Phone number for interface 4 is not known")));
    }

    @Test
    public void testNoOverage() throws Exception {
        // given
        Instant dataLimitStart = Instant.EPOCH.atOffset(ZoneOffset.UTC).withMonth(11).toInstant();
        Instant nowInstant = dataLimitStart.plus(14, ChronoUnit.DAYS);
        setupDataLimits((long) 30e9, dataLimitStart);
        setupInstantSupplier(nowInstant);
        setupActivityResponses(
                dataLimitStart,
                new InterfaceActivity(1, dataLimitStart, nowInstant,
                        mockNetworkActivity((long) 8e9, (long) 8e9)),
                nowInstant,
                new InterfaceActivity(1, dataLimitStart, nowInstant,
                        mockNetworkActivity(1L, 1L)));

        // when
        ResultActions resultActions = sendOverageGet();

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(new JSONArray().toString()));
        mockServer.verify();
    }

    @Test
    public void testOverage() throws Exception {
        // given
        Instant dataLimitStart = Instant.EPOCH.atOffset(ZoneOffset.UTC).withMonth(11).toInstant();
        Instant nowInstant = dataLimitStart.plus(15, ChronoUnit.DAYS).plus(5, ChronoUnit.MINUTES);
        setupDataLimits((long) 30e9, dataLimitStart);
        setupInstantSupplier(nowInstant);
        setupActivityResponses(
                dataLimitStart,
                new InterfaceActivity(1, dataLimitStart, nowInstant,
                        mockNetworkActivity((long) 9e9, (long) 9e9)),
                dataLimitStart.plus(15, ChronoUnit.DAYS),
                new InterfaceActivity(1, dataLimitStart, nowInstant,
                        mockNetworkActivity((long) 0.9e9, 0)));
        when(mockOverageRepo
                .findByOverageTypeAndInstantBetween(any(), any(), any()))
                .thenReturn(Collections.emptyList());

        // when
        ResultActions resultActions = sendOverageGet();

        final String expectedCycleOverageDescription =
                "Cycle usage of 54.0 GB exceeds allowed usage of 48.0 GB "
                + "by more than buffer of 6.0 GB";
        final String expectedHourOverageDescription =
                "Hour usage of 2.70 GB exceeds allowed usage of "
                + "0.90 GB (1.0% of total cycle limit)";
        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(new JSONArray()
                        .put(createOverageJsonObject(
                                expectedCycleOverageDescription,
                                OverageType.CYCLE,
                                nowInstant,
                                null))
                        .put(createOverageJsonObject(
                                expectedHourOverageDescription,
                                OverageType.HOUR,
                                nowInstant,
                                null))
                        .toString()));
        verify(mockOverageRepo).save(new Overage(
                expectedCycleOverageDescription,
                OverageType.CYCLE,
                nowInstant));
        verify(mockOverageRepo).save(new Overage(
                expectedHourOverageDescription,
                OverageType.HOUR,
                nowInstant));
        mockServer.verify();
    }

    @Test
    public void testOverageAfterRecent() throws Exception {
        // given
        Instant dataLimitStart = Instant.EPOCH.atOffset(ZoneOffset.UTC).withMonth(11).toInstant();
        Instant nowInstant = dataLimitStart.plus(15, ChronoUnit.DAYS);
        setupDataLimits((long) 30e9, dataLimitStart);
        setupInstantSupplier(nowInstant);
        setupActivityResponses(
                dataLimitStart,
                new InterfaceActivity(1, dataLimitStart, nowInstant,
                        mockNetworkActivity((long) 9e9, (long) 9e9)),
                nowInstant,
                new InterfaceActivity(1, dataLimitStart, nowInstant,
                        mockNetworkActivity((long) 0.9e9, 0)));
        when(mockOverageRepo
                .findByOverageTypeAndInstantBetween(any(), any(), any()))
                .thenReturn(Collections.singletonList(
                        new Overage("blah", OverageType.CYCLE, nowInstant)));

        // when
        ResultActions resultActions = sendOverageGet();

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(new JSONArray().toString()));
        verify(mockOverageRepo, never()).save(any(Overage.class));
        mockServer.verify();
    }

    @Test
    public void testHourOverageWhenNoCycleOverage() throws Exception {
        // given
        Instant dataLimitStart = Instant.EPOCH.atOffset(ZoneOffset.UTC).withMonth(11).toInstant();
        Instant nowInstant = dataLimitStart.plus(15, ChronoUnit.DAYS);
        setupDataLimits((long) 30e9, dataLimitStart);
        setupInstantSupplier(nowInstant);
        setupActivityResponses(
                dataLimitStart,
                new InterfaceActivity(1, dataLimitStart, nowInstant,
                        mockNetworkActivity((long) 1e9, (long) 1e9)),
                nowInstant,
                new InterfaceActivity(1, dataLimitStart, nowInstant,
                        mockNetworkActivity((long) 0.9e9, 0L)));
        when(mockOverageRepo
                .findByOverageTypeAndInstantBetween(any(), any(), any()))
                .thenReturn(Collections.emptyList());

        // when
        ResultActions resultActions = sendOverageGet();

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(new JSONArray().toString()));
        verify(mockOverageRepo, never()).save(any(Overage.class));
        mockServer.verify();
    }

    @Test
    public void testOverageLastDay() throws Exception {
        // given
        Instant dataLimitStart = Instant.EPOCH.atOffset(ZoneOffset.UTC).withMonth(11).toInstant();
        Instant nowInstant = dataLimitStart.plus(29, ChronoUnit.DAYS);
        setupDataLimits((long) 30e9, dataLimitStart);
        setupInstantSupplier(nowInstant);
        setupActivityResponses(
                dataLimitStart,
                new InterfaceActivity(1, dataLimitStart, nowInstant,
                        mockNetworkActivity((long) 18e9, (long) 18e9)),
                nowInstant,
                new InterfaceActivity(1, dataLimitStart, nowInstant,
                        mockNetworkActivity(1L, 1L)));

        // when
        ResultActions resultActions = sendOverageGet();

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(new JSONArray().toString()));
        verify(mockOverageRepo, never()).save(any(Overage.class));
        mockServer.verify();
    }

    @Test
    public void testOverageByStartStop() throws Exception {
        // given
        final Instant start = Instant.EPOCH;
        final Instant stop = Instant.now();
        when(mockOverageRepo.findByInstantBetween(start, stop)).thenReturn(
                Collections.singletonList(new Overage("blah", OverageType.HOUR, start)));

        // when
        ResultActions resultActions = sendOverageGet(start, stop);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(new JSONArray()
                        .put(createOverageJsonObject("blah", OverageType.HOUR, start, null))
                        .toString()));
        verify(mockOverageRepo, never()).save(any(Overage.class));
        verify(mockOverageRepo).findByInstantBetween(start, stop);
        mockServer.verify();
    }
}
