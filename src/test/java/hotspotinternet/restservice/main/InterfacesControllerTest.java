package hotspotinternet.restservice.main;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Optional;

import org.hamcrest.Matcher;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import hotspotinternet.restservice.entity.AdminStatus;
import hotspotinternet.restservice.entity.DataLimit;
import hotspotinternet.restservice.entity.DataLimit.CyclePeriod;
import hotspotinternet.restservice.entity.DataLimitRepository;
import hotspotinternet.restservice.entity.InterfaceReading;
import hotspotinternet.restservice.entity.InterfaceReadingRepository;
import hotspotinternet.restservice.entity.OperStatus;

@SpringBootTest
@AutoConfigureMockMvc
public class InterfacesControllerTest {
    private static final long INT_MAX_LONG = (long) Integer.MAX_VALUE;
    private static final int INTERFACE_INDEX = 1;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DataLimitRepository mockDataLimitRepo;
    @MockBean
    private InterfaceReadingRepository mockInterfaceReadingRepo;

    private static Matcher<String> embeddedReadingListMatch(final JSONArray expectedJsonArray) {
        return TestUtil.jsonMatcher(
                expectedJsonArray,
                jsonString -> new JSONObject(jsonString)
                        .getJSONObject("_embedded")
                        .getJSONArray("interfaceReadingList"));
    }

    @Test
    public void testPost() throws Exception {
        // given
        final long measurementId = 1;
        final int interfaceIndex = 1;
        final Instant instant = Instant.now();
        final AdminStatus adminStatus = AdminStatus.UP;
        final OperStatus operStatus = OperStatus.DOWN;
        final long inBytes = 4 + INT_MAX_LONG;
        final long inDiscards = 5 + INT_MAX_LONG;
        final long inErrors = 6 + INT_MAX_LONG;
        final long inMulticastPackets = 7 + INT_MAX_LONG;
        final long inUnicastPackets = 8 + INT_MAX_LONG;
        final long inUnknownProtocols = 9 + INT_MAX_LONG;
        final long outBytes = 10 + INT_MAX_LONG;
        final long outDiscards = 11 + INT_MAX_LONG;
        final long outErrors = 12 + INT_MAX_LONG;
        final long outMulticastPackets = 13 + INT_MAX_LONG;
        final long outUnicastPackets = 14 + INT_MAX_LONG;
        // given
        final JSONObject postJson = new JSONObject()
                .put("interfaceIndex", interfaceIndex)
                .put("instant", instant)
                .put("adminStatus", adminStatus.toString())
                .put("operStatus", operStatus.toString())
                .put("inBytes", inBytes)
                .put("inDiscards", inDiscards)
                .put("inErrors", inErrors)
                .put("inMulticastPackets", inMulticastPackets)
                .put("inUnicastPackets", inUnicastPackets)
                .put("inUnknownProtocols", inUnknownProtocols)
                .put("outBytes", outBytes)
                .put("outDiscards", outDiscards)
                .put("outErrors", outErrors)
                .put("outMulticastPackets", outMulticastPackets)
                .put("outUnicastPackets", outUnicastPackets);
        final JSONObject responseJson = new JSONObject(postJson.toString())
                .put("measurementId", measurementId);
        when(mockInterfaceReadingRepo.save(any()))
                .thenReturn(new InterfaceReading(measurementId,
                        interfaceIndex,
                        instant,
                        adminStatus,
                        operStatus,
                        inBytes,
                        inDiscards,
                        inErrors,
                        inMulticastPackets,
                        inUnicastPackets,
                        inUnknownProtocols,
                        outBytes,
                        outDiscards,
                        outErrors,
                        outMulticastPackets,
                        outUnicastPackets));

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder = post("/reading/interface/1")
                .with(TestUtil.authenticated());
        mockRequestBuilder.content(postJson.toString());
        mockRequestBuilder.contentType(MediaType.APPLICATION_JSON);
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(responseJson.toString()));
        verify(mockInterfaceReadingRepo).save(
                new InterfaceReading(null,
                        interfaceIndex,
                        instant,
                        adminStatus,
                        operStatus,
                        inBytes,
                        inDiscards,
                        inErrors,
                        inMulticastPackets,
                        inUnicastPackets,
                        inUnknownProtocols,
                        outBytes,
                        outDiscards,
                        outErrors,
                        outMulticastPackets,
                        outUnicastPackets));
    }

    private static DataLimit createDataLimit(final Instant instant) {
        return DataLimit.recurring(
                INTERFACE_INDEX,
                50L,
                instant,
                CyclePeriod.MONTH);
    }

    private static InterfaceReading createReading(final Instant instant, final long inBytes) {
        return new InterfaceReading(0L,
                INTERFACE_INDEX,
                instant,
                AdminStatus.DOWN,
                OperStatus.DORMANT,
                inBytes,
                0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L, 0L);
    }

    private static JSONObject createReadingJsonObject(
            final Instant instant,
            final long inBytes) {
        return new JSONObject()
                .put("measurementId", JSONObject.NULL)
                .put("interfaceIndex", INTERFACE_INDEX)
                .put("instant", instant.toString())
                .put("adminStatus", AdminStatus.DOWN)
                .put("operStatus", OperStatus.DORMANT)
                .put("inBytes", inBytes)
                .put("inDiscards", 0)
                .put("inErrors", 0)
                .put("inMulticastPackets", 0)
                .put("inUnicastPackets", 0)
                .put("inUnknownProtocols", 0)
                .put("outBytes", 0)
                .put("outDiscards", 0)
                .put("outErrors", 0)
                .put("outMulticastPackets", 0)
                .put("outUnicastPackets", 0);
    }

    private static JSONObject createActivityModelJsonObject(
            JSONObject linksObject,
            JSONObject... activityModels) {
        JSONArray jsonArray = new JSONArray();
        for (JSONObject activityModel : activityModels) {
            jsonArray.put(activityModel);
        }
        return new JSONObject()
                .put("_embedded", new JSONObject().put(
                        "interfaceActivityList", jsonArray))
                .put("_links", linksObject);
    }

    private static JSONObject createLinksObject(
            final String previousHref,
            final String selfHref,
            final String nextHref,
            final String intervalInHref,
            final String intervalContainsHref) {
        return new JSONObject()
                .put("intervalIn", new JSONObject().put(
                        "href", intervalInHref))
                .put("intervalContains", new JSONObject().put(
                        "href", intervalContainsHref))
                .put("prev", new JSONObject().put(
                        "href", previousHref))
                .put("self", new JSONObject().put(
                        "href", selfHref))
                .put("next", new JSONObject().put(
                        "href", nextHref));
    }

    private static JSONObject createActivityJsonObject(
            final Instant startInstant,
            final Instant stopInstant,
            final long inBytes,
            final JSONObject linksObject) {
        return new JSONObject()
                .put("interfaceIndex", INTERFACE_INDEX)
                .put("startInstant", startInstant.toString())
                .put("stopInstant", stopInstant.toString())
                .put("inBytes", inBytes)
                .put("inDiscards", 0)
                .put("inErrors", 0)
                .put("inMulticastPackets", 0)
                .put("inUnicastPackets", 0)
                .put("inUnknownProtocols", 0)
                .put("outBytes", 0)
                .put("outDiscards", 0)
                .put("outErrors", 0)
                .put("outMulticastPackets", 0)
                .put("outUnicastPackets", 0)
                .put("_links", linksObject);
    }

    @Test
    public void testGetWhenOneReading() throws Exception {
        // given
        when(mockDataLimitRepo.findByInterfaceIndex(INTERFACE_INDEX)).thenReturn(
                Arrays.asList(
                        createDataLimit(Instant.EPOCH)));
        when(mockInterfaceReadingRepo
                .findByInterfaceIndexAndInstantBetween(
                        anyInt(),
                        any(Instant.class),
                        any(Instant.class)))
                .thenReturn(Arrays.asList(
                        createReading(Instant.EPOCH.plusMillis(100), 2)));
        when(mockInterfaceReadingRepo
                .findFirstByInterfaceIndexOrderByInstantDesc(INTERFACE_INDEX))
                .thenReturn(Optional.of(
                        createReading(Instant.EPOCH.plusMillis(100), 2)));

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder = get("/reading/interface/1");
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        final JSONArray responseJsonArray = new JSONArray()
                .put(createReadingJsonObject(
                        Instant.EPOCH.plusMillis(100),
                        0));
        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(embeddedReadingListMatch(responseJsonArray)));
    }

    @Test
    public void testGetReadingWhenNoDataLimits() throws Exception {
        // given
        when(mockInterfaceReadingRepo
                .findFirstByInterfaceIndexOrderByInstantDesc(INTERFACE_INDEX))
                .thenReturn(Optional.of(
                        createReading(Instant.EPOCH, 2)));
        when(mockDataLimitRepo.findByInterfaceIndex(INTERFACE_INDEX)).thenReturn(Arrays.asList());

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder = get("/reading/interface/1");
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString("No data limit relevant")));
    }

    @Test
    public void testGetWhenNoReadings() throws Exception {
        // given
        when(mockDataLimitRepo.findByInterfaceIndex(INTERFACE_INDEX)).thenReturn(
                Arrays.asList(
                        createDataLimit(Instant.EPOCH)));
        when(mockInterfaceReadingRepo
                .findByInterfaceIndexAndInstantBetween(
                        anyInt(),
                        any(Instant.class),
                        any(Instant.class)))
                .thenReturn(Arrays.asList());
        when(mockInterfaceReadingRepo
                .findFirstByInterfaceIndexOrderByInstantDesc(INTERFACE_INDEX))
                .thenReturn(Optional.of(
                        createReading(Instant.EPOCH, 2)));

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder = get(
                "/reading/interface/1?start=1970-01-15T23:59:59.999Z");
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString("No readings available")));
    }

    @Test
    public void testGetReadingWithReset() throws Exception {
        // given
        final Instant epochMinusMonth = Instant.EPOCH
                .atOffset(ZoneOffset.UTC)
                .minusMonths(1)
                .toInstant();
        when(mockDataLimitRepo.findByInterfaceIndex(INTERFACE_INDEX)).thenReturn(
                Arrays.asList(
                        createDataLimit(epochMinusMonth.plusNanos(1)),
                        createDataLimit(epochMinusMonth)));
        when(mockInterfaceReadingRepo
                .findByInterfaceIndexAndInstantBetween(
                        anyInt(),
                        any(Instant.class),
                        any(Instant.class)))
                .thenReturn(Arrays.asList(
                        // readings intentionally provided out-of-order (i.e. 200ms offset
                        // comes after 350ms offset) to make sure readings are being sorted
                        createReading(Instant.EPOCH.minusMillis(100), 2),
                        createReading(Instant.EPOCH.plusMillis(100), 2),
                        createReading(Instant.EPOCH.plusMillis(250), 2),
                        createReading(Instant.EPOCH.plusMillis(350), 2),
                        createReading(Instant.EPOCH.plusMillis(200), 4)));
        when(mockInterfaceReadingRepo
                .findFirstByInterfaceIndexOrderByInstantDesc(INTERFACE_INDEX))
                .thenReturn(Optional.of(
                        createReading(Instant.EPOCH.plusMillis(350), 2)));
        final JSONArray responseJsonArray = new JSONArray()
                .put(createReadingJsonObject(
                        Instant.EPOCH.plusMillis(350),
                        4));

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder = get("/reading/interface/1");
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(embeddedReadingListMatch(responseJsonArray)));
        // two data limits were mocked for return, but the data limit with the start instant
        // that is earlier by 1 nanosecond (i.e. EPOCH) is the relevant one
        Instant expectedFindInstant = Instant.EPOCH
                .atOffset(ZoneOffset.UTC)
                .minusDays(1)
                .toInstant();
        verify(mockInterfaceReadingRepo).findByInterfaceIndexAndInstantBetween(
                eq(INTERFACE_INDEX),
                eq(expectedFindInstant),
                any());
    }

    private Instant epochWithDayOfMonth(final int dayOfMonth) {
        return Instant.EPOCH
                .atOffset(ZoneOffset.UTC)
                .withDayOfMonth(dayOfMonth)
                .toInstant();
    }

    private Instant epochWithHourAndMinute(final int hour, final int minute) {
        return Instant.EPOCH
                .atOffset(ZoneOffset.UTC)
                .withHour(hour)
                .withMinute(minute)
                .toInstant();
    }

    private Instant epochWithDayAndHour(final int dayOfMonth, final int hour) {
        return Instant.EPOCH
                .atOffset(ZoneOffset.UTC)
                .withDayOfMonth(dayOfMonth)
                .withHour(hour)
                .toInstant();
    }

    @Test
    public void testGetReadingsCycleStart() throws Exception {
        // given
        DataLimit dataLimit = createDataLimit(Instant.EPOCH
                .atOffset(ZoneOffset.UTC)
                .withDayOfMonth(19)
                .minusMonths(1)
                .toInstant());
        when(mockDataLimitRepo.findByInterfaceIndex(INTERFACE_INDEX)).thenReturn(
                Arrays.asList(dataLimit));
        when(mockInterfaceReadingRepo
                .findByInterfaceIndexAndInstantBetween(
                        anyInt(),
                        any(Instant.class),
                        any(Instant.class)))
                .thenReturn(Arrays.asList(
                        createReading(
                                epochWithDayAndHour(18, 12),
                                1),
                        createReading(
                                epochWithDayAndHour(18, 18),
                                2),
                        // at 19th day of month, should interpolate to inBytes=4
                        createReading(
                                epochWithDayAndHour(20, 0),
                                10)));
        when(mockInterfaceReadingRepo
                .findFirstByInterfaceIndexOrderByInstantDesc(INTERFACE_INDEX))
                .thenReturn(Optional.of(
                        createReading(epochWithDayAndHour(20, 0), 10)));
        final JSONArray responseJsonArray = new JSONArray()
                .put(createReadingJsonObject(
                        epochWithDayOfMonth(19),
                        0))
                .put(createReadingJsonObject(
                        epochWithDayOfMonth(20),
                        6));
        final MockHttpServletRequestBuilder mockRequestBuilder = get(
                String.format(
                        "/reading/interface/1?start=%s&quantity=2",
                        epochWithDayOfMonth(19)));
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(embeddedReadingListMatch(responseJsonArray)));
        verify(mockInterfaceReadingRepo).findByInterfaceIndexAndInstantBetween(
                eq(INTERFACE_INDEX),
                eq(epochWithDayOfMonth(18)
                        .atOffset(ZoneOffset.UTC)
                        .minusMonths(1)
                        .toInstant()),
                any());
    }

    @Test
    public void testGetReadingCycleEnd() throws Exception {
        // given
        DataLimit dataLimit = createDataLimit(Instant.EPOCH
                .atOffset(ZoneOffset.UTC)
                .withDayOfMonth(19)
                .minusMonths(3)
                .toInstant());
        when(mockDataLimitRepo.findByInterfaceIndex(INTERFACE_INDEX)).thenReturn(
                Arrays.asList(dataLimit));
        when(mockInterfaceReadingRepo
                .findByInterfaceIndexAndInstantBetween(
                        anyInt(),
                        any(Instant.class),
                        any(Instant.class)))
                .thenReturn(Arrays.asList(
                        createReading(
                                epochWithDayAndHour(16, 0),
                                1),
                        createReading(
                                epochWithDayAndHour(17, 0),
                                2),
                        // at 12:00 on 18th day of month, should interpolate to inBytes=5
                        createReading(
                                epochWithDayAndHour(19, 0),
                                6)));
        when(mockInterfaceReadingRepo
                .findFirstByInterfaceIndexOrderByInstantDesc(INTERFACE_INDEX))
                .thenReturn(Optional.of(
                        createReading(epochWithDayAndHour(19, 0), 6)));
        final JSONArray responseJsonArray = new JSONArray()
                .put(createReadingJsonObject(
                        epochWithDayAndHour(18, 12),
                        4));
        final MockHttpServletRequestBuilder mockRequestBuilder = get(
                String.format(
                        "/reading/interface/1?start=%s&quantity=1",
                        epochWithDayAndHour(18, 12)));
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(embeddedReadingListMatch(responseJsonArray)));
    }

    @Test
    public void testGetActivity() throws Exception {
        // given
        DataLimit dataLimit = createDataLimit(Instant.EPOCH
                .atOffset(ZoneOffset.UTC)
                .withDayOfMonth(19)
                .minusMonths(1)
                .toInstant()
                .minusMillis(1));
        when(mockDataLimitRepo.findByInterfaceIndex(INTERFACE_INDEX)).thenReturn(
                Arrays.asList(dataLimit));
        when(mockInterfaceReadingRepo
                .findByInterfaceIndexAndInstantBetween(
                        anyInt(),
                        any(Instant.class),
                        any(Instant.class)))
                .thenReturn(Arrays.asList(
                        createReading(epochWithDayOfMonth(18), 2),
                        createReading(epochWithDayOfMonth(19), 4),
                        createReading(epochWithDayOfMonth(21), 6),
                        createReading(epochWithDayOfMonth(22), 3)));
        when(mockInterfaceReadingRepo
                .findFirstByInterfaceIndexOrderByInstantDesc(INTERFACE_INDEX))
                .thenReturn(Optional.of(
                        createReading(epochWithDayOfMonth(22), 3)));

        JSONObject collectionModelLinks = createLinksObject(
                // previous
                String.format(
                        "http://localhost/activity/interface/1?divided=false&start=%s&period=DAY&quantity=3",
                        epochWithDayOfMonth(16)),
                // self
                String.format(
                        "http://localhost/activity/interface/1?divided=false&start=%s&period=DAY&quantity=3",
                        epochWithDayOfMonth(19)),
                // nexf
                String.format(
                        "http://localhost/activity/interface/1?divided=false&start=%s&period=DAY&quantity=3",
                        epochWithDayOfMonth(22)),
                // interval in
                String.format(
                        "http://localhost/activity/interface/1?divided=true&start=%s&period=MONTH&quantity=1",
                        epochWithDayOfMonth(19).minusMillis(1)),
                // interval contains
                String.format(
                        "http://localhost/activity/interface/1?divided=true&start=%s&period=DAY&quantity=3",
                        epochWithDayOfMonth(19)));

        String expectedSelfFormat =
                "http://localhost/activity/interface/1?divided=false&start=%s&period=DAY&quantity=1";
        String expectedIntervalIn = String.format(
                "http://localhost/activity/interface/1?divided=true&start=%s&period=MONTH&quantity=1",
                epochWithDayOfMonth(19).minusMillis(1));
        String intervalContainsFormat =
                "http://localhost/activity/interface/1?divided=true&start=%s&period=DAY&quantity=1";
        final JSONObject responseObject = createActivityModelJsonObject(
                collectionModelLinks,
                createActivityJsonObject(
                        epochWithDayOfMonth(19),
                        epochWithDayOfMonth(20),
                        1,
                        createLinksObject(
                                String.format(expectedSelfFormat, epochWithDayOfMonth(18)),
                                String.format(expectedSelfFormat, epochWithDayOfMonth(19)),
                                String.format(expectedSelfFormat, epochWithDayOfMonth(20)),
                                expectedIntervalIn,
                                String.format(intervalContainsFormat, epochWithDayOfMonth(19)))),
                createActivityJsonObject(
                        epochWithDayOfMonth(20),
                        epochWithDayOfMonth(21),
                        1,
                        createLinksObject(
                                String.format(expectedSelfFormat, epochWithDayOfMonth(19)),
                                String.format(expectedSelfFormat, epochWithDayOfMonth(20)),
                                String.format(expectedSelfFormat, epochWithDayOfMonth(21)),
                                expectedIntervalIn,
                                String.format(intervalContainsFormat, epochWithDayOfMonth(20)))),
                createActivityJsonObject(
                        epochWithDayOfMonth(21),
                        epochWithDayOfMonth(22),
                        3,
                        createLinksObject(
                                String.format(expectedSelfFormat, epochWithDayOfMonth(20)),
                                String.format(expectedSelfFormat, epochWithDayOfMonth(21)),
                                String.format(expectedSelfFormat, epochWithDayOfMonth(22)),
                                expectedIntervalIn,
                                String.format(intervalContainsFormat, epochWithDayOfMonth(21)))));

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder = get(
                String.format(
                        "/activity/interface/1?start=%s&period=DAY&quantity=3",
                        epochWithDayOfMonth(19)));
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(responseObject.toString(), true));
        verify(mockInterfaceReadingRepo).findByInterfaceIndexAndInstantBetween(
                eq(INTERFACE_INDEX),
                eq(epochWithDayOfMonth(18)),
                any());
    }

    @Test
    public void testGetInvalidQuantity() throws Exception {
        // when
        final MockHttpServletRequestBuilder mockRequestBuilder =
                get("/reading/interface/1?quantity=0");
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(content().string("'quantity' must be positive integer"));
    }

    @Test
    public void testGetMultipleWithHourInterval() throws Exception {
        // given
        when(mockDataLimitRepo.findByInterfaceIndex(INTERFACE_INDEX)).thenReturn(
                Arrays.asList(
                        createDataLimit(
                                Instant.EPOCH
                                .minusMillis(1))));
        when(mockInterfaceReadingRepo
                .findByInterfaceIndexAndInstantBetween(
                        anyInt(),
                        any(Instant.class),
                        any(Instant.class)))
                .thenReturn(Arrays.asList(
                        createReading(epochWithHourAndMinute(0, 0), 3),
                        createReading(epochWithHourAndMinute(0, 59), 3),
                        createReading(epochWithHourAndMinute(1, 1), 3),
                        createReading(epochWithHourAndMinute(1, 2), 4),
                        createReading(epochWithHourAndMinute(1, 3), 2),
                        createReading(epochWithHourAndMinute(1, 59), 4),
                        createReading(epochWithHourAndMinute(2, 1), 4)));
        final JSONArray responseJsonArray = new JSONArray()
                .put(createReadingJsonObject(
                        epochWithHourAndMinute(1, 0),
                        0))
                .put(createReadingJsonObject(
                        epochWithHourAndMinute(2, 0),
                        5));

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder = get(
                String.format(
                        "/reading/interface/1?start=%s&interval=HOUR&quantity=4",
                        epochWithHourAndMinute(1, 0)));
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(embeddedReadingListMatch(responseJsonArray)));
        verify(mockInterfaceReadingRepo).findByInterfaceIndexAndInstantBetween(
                eq(INTERFACE_INDEX),
                eq(epochWithHourAndMinute(0, 0)
                        .atOffset(ZoneOffset.UTC)
                        .minusHours(1)
                        .toInstant()
                        .minusMillis(1)),
                any());
    }
}
