package hotspotinternet.restservice.main;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.Instant;
import java.util.Arrays;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import hotspotinternet.restservice.entity.DataLimit;
import hotspotinternet.restservice.entity.DataLimit.CyclePeriod;
import hotspotinternet.restservice.entity.DataLimitRepository;
import hotspotinternet.restservice.entity.InterfaceReadingRepository;

@SpringBootTest
@AutoConfigureMockMvc
public class DataLimitControllerTest {
    private static final long BYTES = 1;
    private static final int INTERFACE_INDEX = 2;
    private static final Instant START_INSTANT = Instant.now().minusSeconds(100);
    private static final Instant EXPIRED_LIMIT_STOP_INSTANT = START_INSTANT.minusSeconds(1000);
    private static final CyclePeriod CYCLE_PERIOD = CyclePeriod.MONTH;
    private static final DataLimit DATA_LIMIT = DataLimit.recurring(
            INTERFACE_INDEX,
            BYTES,
            START_INSTANT,
            CYCLE_PERIOD);
    private static final DataLimit EXPIRED_DATA_LIMIT = DataLimit.recurringWithStop(
            INTERFACE_INDEX,
            BYTES,
            Instant.EPOCH,
            START_INSTANT.minusSeconds(1000),
            CYCLE_PERIOD);
    private static final JSONObject DATA_LIMIT_JSON = new JSONObject()
            .put("dataLimitId", JSONObject.NULL)
            .put("interfaceIndex", INTERFACE_INDEX)
            .put("bytes", BYTES)
            .put("startInstant", START_INSTANT)
            .put("stopInstant", JSONObject.NULL)
            .put("cyclePeriod", CYCLE_PERIOD);
    private static final JSONObject EXPIRED_DATA_LIMIT_JSON =
            new JSONObject(DATA_LIMIT_JSON.toString())
            .put("startInstant", Instant.EPOCH)
            .put("stopInstant", EXPIRED_LIMIT_STOP_INSTANT);

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DataLimitRepository mockDataLimitRepo;
    @MockBean
    private InterfaceReadingRepository mockInterfaceReadingRepo;

    private static JSONArray createJsonArray(JSONObject... jsonObjects) {
        final JSONArray jsonArray = new JSONArray();
        for (JSONObject jsonObject : jsonObjects) {
            jsonArray.put(jsonObject);
        }
        return jsonArray;
    }

    @Test
    public void testPost() throws Exception {
        // given
        final long bytes = 1;
        final int interfaceIndex = 2;
        final Instant startInstant = Instant.now();
        final Instant stopInstant = Instant.now().plusMillis(100);
        final CyclePeriod cyclePeriod = CyclePeriod.MONTH;

        final JSONObject postJson = new JSONObject()
                .put("dataLimitId", JSONObject.NULL)
                .put("interfaceIndex", interfaceIndex)
                .put("bytes", bytes)
                .put("startInstant", startInstant)
                .put("stopInstant", stopInstant)
                .put("cyclePeriod", cyclePeriod);
        DataLimit dataLimit = DataLimit.recurringWithStop(
                interfaceIndex,
                bytes,
                startInstant,
                stopInstant,
                cyclePeriod);
        when(mockDataLimitRepo.save(any())).thenReturn(dataLimit);

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder = post("/datalimit/interface/2")
                .with(TestUtil.authenticated());
        mockRequestBuilder.content(postJson.toString());
        mockRequestBuilder.contentType(MediaType.APPLICATION_JSON);
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().json(postJson.toString()));
        verify(mockDataLimitRepo).save(dataLimit);
    }

    @Test
    public void testGet() throws Exception {
        // given
        when(mockDataLimitRepo.findByInterfaceIndex(INTERFACE_INDEX))
                .thenReturn(Arrays.asList(DATA_LIMIT, EXPIRED_DATA_LIMIT));

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder = get("/datalimit/interface/2");
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(TestUtil.jsonMatcher(createJsonArray(
                        DATA_LIMIT_JSON))));
    }

    @Test
    public void testGetAll() throws Exception {
        // given
        when(mockDataLimitRepo.findByInterfaceIndex(INTERFACE_INDEX))
                .thenReturn(Arrays.asList(DATA_LIMIT, EXPIRED_DATA_LIMIT));

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder =
                get("/datalimit/interface/2?all=true");
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(TestUtil.jsonMatcher(createJsonArray(
                        DATA_LIMIT_JSON,
                        EXPIRED_DATA_LIMIT_JSON))));
    }

    @Test
    public void testGetInPast() throws Exception {
        // given
        when(mockDataLimitRepo.findByInterfaceIndex(INTERFACE_INDEX))
                .thenReturn(Arrays.asList(DATA_LIMIT, EXPIRED_DATA_LIMIT));

        // when
        final MockHttpServletRequestBuilder mockRequestBuilder = get(String.format(
                "/datalimit/interface/2?instant=%s",
                EXPIRED_LIMIT_STOP_INSTANT.minusSeconds(5)));
        ResultActions resultActions = mockMvc.perform(mockRequestBuilder);

        // then
        resultActions
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(TestUtil.jsonMatcher(createJsonArray(
                        EXPIRED_DATA_LIMIT_JSON))));
    }
}
