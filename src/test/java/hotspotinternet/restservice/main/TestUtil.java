package hotspotinternet.restservice.main;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;

import java.util.function.Function;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.security.core.Authentication;
import org.springframework.test.web.servlet.request.RequestPostProcessor;

public class TestUtil {

    public static Matcher<String> jsonMatcher(final JSONArray expectedJsonArray) {
        return jsonMatcher(expectedJsonArray, JSONArray::new);
    }

    public static Matcher<String> jsonMatcher(
            final JSONArray expectedJsonArray,
            final Function<String, JSONArray> arrayFunction) {
        return new BaseMatcher<String>() {
            private int mismatchedIndex;
            private String mismatchedKey;
            private Object actualValue;

            @Override
            public boolean matches(Object actual) {
                if (!(actual instanceof String)) {
                    return false;
                }
                JSONArray actualJsonArray = arrayFunction.apply((String) actual);
                if (actualJsonArray.length() != expectedJsonArray.length()) {
                    return false;
                }
                for (int index = 0; index < expectedJsonArray.length(); ++index) {
                    JSONObject expectedJsonObject = (JSONObject) expectedJsonArray.get(index);
                    JSONObject actualJsonObject = (JSONObject) actualJsonArray.get(index);
                    for (String key : expectedJsonObject.keySet()) {
                        if (!expectedJsonObject.get(key).toString().equals(
                                actualJsonObject.get(key).toString())) {
                            mismatchedIndex = index;
                            mismatchedKey = key;
                            actualValue = actualJsonObject.get(key);
                            return false;
                        }
                    }
                }
                return true;
            }

            @Override
            public void describeMismatch(Object item, Description description) {
                description
                        .appendText(String.format(
                                "values for '%s' should match (index=%s). %s expected, but was: ",
                                mismatchedKey,
                                mismatchedIndex,
                                ((JSONObject) expectedJsonArray.get(mismatchedIndex))
                                        .get(mismatchedKey)))
                        .appendValue(actualValue);
            }

            @Override
            public void describeTo(Description description) {
                // do nothing
            }
        };
    }

    public static RequestPostProcessor authenticated() {
        Authentication mockAuthentication = mock(Authentication.class);
        when(mockAuthentication.isAuthenticated()).thenReturn(true);
        return authentication(mockAuthentication);
    }
}
