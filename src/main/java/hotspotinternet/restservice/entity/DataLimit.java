package hotspotinternet.restservice.entity;

import static com.google.common.base.Preconditions.checkNotNull;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Optional;

/**
 * Class for storing information of a data limit.
 */
@Entity
public final class DataLimit {

    /**
     * Cycle periods for defining the 'reset' of a data limit cycle.
     */
    public enum CyclePeriod {
        /** A month. i.e. from N-th day of month to N-th day of next month. */
        MONTH,
        /** A day. i.e. 24 hours. */
        DAY;
    }

    private @Id @GeneratedValue Long dataLimitId;
    private Integer interfaceIndex;
    private Long bytes;
    private Instant startInstant;
    private Instant stopInstant;
    private CyclePeriod cyclePeriod;

    // Empty, no-arg constructor for JSON mapping
    public DataLimit() { }

    private DataLimit(
            final Integer interfaceIndex,
            final Long bytes,
            final Instant startInstant,
            final Instant stopInstant,
            final CyclePeriod cyclePeriod) {
        this.interfaceIndex = checkNotNull(interfaceIndex);
        this.bytes = checkNotNull(bytes);
        this.startInstant = checkNotNull(startInstant);
        this.stopInstant = stopInstant;
        this.cyclePeriod = cyclePeriod;
    }

    /**
     * Creates recurring data limit (with no stop) from provided arguments.
     *
     * @param interfaceIndex of data limit
     * @param bytes of data limit
     * @param startInstant for start of data limit
     * @param cyclePeriod for defining the 'reset' of data cycles
     * @return DataLimit instantiated from provided arguments
     */
    @VisibleForTesting
    public static DataLimit recurring(
            final Integer interfaceIndex,
            final Long bytes,
            final Instant startInstant,
            final CyclePeriod cyclePeriod) {
        return recurringWithStop(interfaceIndex,
                bytes,
                startInstant,
                null,
                cyclePeriod);
    }

    /**
     * Creates recurring data limit from provided arguments.
     *
     * @param interfaceIndex of data limit
     * @param bytes of data limit
     * @param startInstant for start of data limit
     * @param stopInstant for stop of data limit
     * @param cyclePeriod for defining the 'reset' of data cycles
     * @return DataLimit instantiated from provided arguments
     */
    @VisibleForTesting
    public static DataLimit recurringWithStop(
            final Integer interfaceIndex,
            final Long bytes,
            final Instant startInstant,
            final Instant stopInstant,
            final CyclePeriod cyclePeriod) {
        return new DataLimit(interfaceIndex,
                bytes,
                startInstant,
                stopInstant,
                checkNotNull(cyclePeriod));
    }

    @VisibleForTesting
    static DataLimit oneShot(
            final Integer interfaceIndex,
            final Long bytes,
            final Instant startInstant,
            final Instant stopInstant) {
        return new DataLimit(interfaceIndex, bytes, startInstant, checkNotNull(stopInstant), null);
    }

    /**
     * Determines whether provided instant is within the timeframe of this DataLimit.
     *
     * @param instant of interest
     * @return true if provided instant is within timeframe of this Data limit,
     *     false otherwise
     */
    public boolean isFor(final Instant instant) {
        if (instant.isBefore(startInstant)) {
            return false;
        }
        return stopInstant == null
                ? true
                : instant.isBefore(stopInstant);
    }

    private Instant getMonthlyCycleStart(final Instant instant) {
        ZonedDateTime instantDateTime = instant
                .atZone(ZoneOffset.UTC);
        ZonedDateTime cycleStartThisMonth = startInstant
                .atZone(ZoneOffset.UTC)
                .withMonth(instantDateTime.getMonthValue())
                .withYear(instantDateTime.getYear());
        if (cycleStartThisMonth.isBefore(instantDateTime)) {
            return cycleStartThisMonth.toInstant();
        }
        if (cycleStartThisMonth.getMonthValue() == 1) {
            return cycleStartThisMonth
                    .withMonth(12)
                    .withYear(cycleStartThisMonth.getYear() - 1)
                    .toInstant();
        }
        return cycleStartThisMonth
                .withMonth(cycleStartThisMonth.getMonthValue() - 1)
                .toInstant();
    }

    public Integer getInterfaceIndex() {
        return interfaceIndex;
    }

    public Long getDataLimitId() {
        return dataLimitId;
    }

    public Long getBytes() {
        return bytes;
    }

    public Instant getStartInstant() {
        return startInstant;
    }

    /**
     * Gets start instant for the provided instant.
     *
     * @param instant for which DataLimit start instant is needed
     * @return start instant for provided instant or absent Optional
     *     if instant is after the stop instant for DataLimit
     */
    public Optional<Instant> getStartInstant(final Instant instant) {
        if (!isFor(instant)) {
            return Optional.absent();
        }
        if (cyclePeriod == null) {
            return Optional.of(startInstant);
        }
        switch (cyclePeriod) {
            case MONTH:
                return Optional.of(getMonthlyCycleStart(instant));
            case DAY:
            default:
                throw new IllegalArgumentException("Unhandled cycle period: " + cyclePeriod);
        }
    }

    public Instant getStopInstant() {
        return stopInstant;
    }

    /**
     * Gets stop instant for the provided instant.
     *
     * @param instant for which DataLimit stop instant is needed
     * @return stop instant for provided instant or absent Optional
     *     if instant is after the stop instant for DataLimit
     */
    public Optional<Instant> getStopInstant(final Instant instant) {
        Optional<Instant> startInstantOptional = getStartInstant(instant);
        if (!startInstantOptional.isPresent()) {
            return Optional.absent();
        }

        switch (cyclePeriod) {
            case MONTH:
                final Instant endOfMonthCycle = startInstantOptional.get()
                        .atOffset(ZoneOffset.UTC)
                        .plusMonths(1)
                        .toInstant();
                return Optional.of(
                        stopInstant != null && stopInstant.isBefore(endOfMonthCycle)
                        ? stopInstant
                        : endOfMonthCycle);
            case DAY:
            default:
                throw new IllegalArgumentException("Unhandled cycle period: " + cyclePeriod);
        }
    }

    public CyclePeriod getCyclePeriod() {
        return cyclePeriod;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("Data Limit ID", dataLimitId)
                .add("Bytes", bytes)
                .add("Start Instant", startInstant)
                .add("Stop Instant", stopInstant)
                .add("Cycle Period", cyclePeriod)
                .toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof DataLimit)) {
            return false;
        }
        DataLimit otherDataLimit = (DataLimit) other;
        return Objects.equal(dataLimitId, otherDataLimit.dataLimitId)
                && Objects.equal(bytes, otherDataLimit.bytes)
                && Objects.equal(startInstant, otherDataLimit.startInstant)
                && Objects.equal(stopInstant, otherDataLimit.stopInstant)
                && Objects.equal(cyclePeriod, otherDataLimit.cyclePeriod);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(
                dataLimitId,
                bytes,
                startInstant,
                stopInstant,
                cyclePeriod);
    }
}
