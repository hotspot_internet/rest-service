package hotspotinternet.restservice.entity;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

/**
 * Repository for storing/retrieving instances of UsageReport.
 */
public interface UsageReportRepository extends CrudRepository<UsageReport, Long> {
    Optional<UsageReport> findFirstByMessageId(String messageId);

    List<UsageReport> findByPhoneNumberContaining(String phoneNumber);
}
