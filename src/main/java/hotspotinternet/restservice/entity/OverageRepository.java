package hotspotinternet.restservice.entity;

import java.time.Instant;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import hotspotinternet.restservice.entity.Overage.OverageType;

/**
 * Repository for storing/retrieving instances of Overage.
 */
public interface OverageRepository extends CrudRepository<Overage, Long> {
    List<Overage> findByInstantBetween(Instant start, Instant stop);

    List<Overage> findByOverageTypeAndInstantBetween(
            OverageType overageType,
            Instant start,
            Instant stop);
}
