package hotspotinternet.restservice.entity;

import static com.google.common.base.Preconditions.checkArgument;

import java.time.Instant;
import java.util.function.BiFunction;
import java.util.function.Function;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Class for storing information of an interface reading.
 */
@Entity
public final class InterfaceReading extends NetworkActivity {

    private @Id @GeneratedValue Long measurementId;
    private Integer interfaceIndex;
    private Instant instant;
    private AdminStatus adminStatus;
    private OperStatus operStatus;

    /**
     * Default constructor.
     */
    public InterfaceReading() {}

    /**
     * Construct InterfaceReading with provided arguments.
     *
     * @param measurementId measurement ID
     * @param interfaceIndex index of interface
     * @param instant when reading taken
     * @param adminStatus admin status
     * @param operStatus operation status
     * @param inBytes number of bytes received
     * @param inDiscards in-packets discarded
     * @param inErrors in-packet errors
     * @param inMulticastPackets multicast packets received
     * @param inUnicastPackets unicast packets received
     * @param inUnknownProtocols packets received with unknown protocol
     * @param outBytes number of bytes sent out
     * @param outDiscards out-packets discarded
     * @param outErrors out-packet errors
     * @param outMulticastPackets multicast packets sent
     * @param outUnicastPackets unicast packets sent
     */
    public InterfaceReading(
            final Long measurementId,
            final int interfaceIndex,
            final Instant instant,
            final AdminStatus adminStatus,
            final OperStatus operStatus,
            final Long inBytes,
            final Long inDiscards,
            final Long inErrors,
            final Long inMulticastPackets,
            final Long inUnicastPackets,
            final Long inUnknownProtocols,
            final Long outBytes,
            final Long outDiscards,
            final Long outErrors,
            final Long outMulticastPackets,
            final Long outUnicastPackets) {
        super(
                inBytes, inDiscards, inErrors,
                inMulticastPackets, inUnicastPackets, inUnknownProtocols,
                outBytes, outDiscards, outErrors,
                outMulticastPackets, outUnicastPackets);
        this.interfaceIndex = interfaceIndex;
        this.instant = instant;
        this.measurementId = measurementId;
        this.adminStatus = adminStatus;
        this.operStatus = operStatus;
    }

    /**
     * Estimates reading at specified instant using provided readings.
     * Estimates calculated for all Number fields. For non-Number fields (e.g. admin status),
     * the value of the reading (i.e. readingA or readingB) that is closest to instant of estimate
     * is used. Measurement ID is set to null since this reading no longer represents
     * an actual reading.
     *
     * @param readingA first of two readings for estimating reading at instant
     * @param readingB second of two readings for estimating reading at instant
     * @param instant at which to estimate reading
     * @return estimated reading
     * @throws IllegalArgumentException if provided readings are at same instant
     */
    public static InterfaceReading getEstimatedReading(
            final InterfaceReading readingA,
            final InterfaceReading readingB,
            final Instant instant) {
        checkArgument(
                !readingB.getInstant().equals(readingA.getInstant()),
                "Provided readings must not be at the same instant");

        final double m = (double) (instant.toEpochMilli() - readingA.instant.toEpochMilli())
                / (readingB.instant.toEpochMilli() - readingA.instant.toEpochMilli());
        final BiFunction<Long, Long, Long> interpolator = (longA, longB) ->
                longA + Math.round((longB - longA) * m);

        return new InterfaceReading(
                null,
                m > 0.5 ? readingB.interfaceIndex : readingA.interfaceIndex,
                instant,
                m > 0.5 ? readingB.adminStatus : readingA.adminStatus,
                m > 0.5 ? readingB.operStatus : readingA.operStatus,
                interpolator.apply(readingA.inBytes, readingB.inBytes),
                interpolator.apply(readingA.inDiscards, readingB.inDiscards),
                interpolator.apply(readingA.inErrors, readingB.inErrors),
                interpolator.apply(
                        readingA.inMulticastPackets,
                        readingB.inMulticastPackets),
                interpolator.apply(
                        readingA.inUnicastPackets,
                        readingB.inUnicastPackets),
                interpolator.apply(
                        readingA.inUnknownProtocols,
                        readingB.inUnknownProtocols),
                interpolator.apply(readingA.outBytes, readingB.outBytes),
                interpolator.apply(readingA.outDiscards, readingB.outDiscards),
                interpolator.apply(readingA.outErrors, readingB.outErrors),
                interpolator.apply(
                        readingA.outMulticastPackets,
                        readingB.outMulticastPackets),
                interpolator.apply(
                        readingA.outUnicastPackets,
                        readingB.outUnicastPackets));
    }

    /**
     * Creates a reading that is the result of subtracting Number fields of
     * provided reading from corresponding fields of this reading. Non-Number
     * fields are set to the values of this reading.
     *
     * @param other reading with values to be subtracted from this reading
     * @return result of subtracting Number fields of provided reading from
     *      this reading
     */
    public InterfaceReading substract(
            final InterfaceReading other) {
        return adjustWith(other, (long1, long2) -> long1 - long2);
    }

    /**
     * Creates a reading that is the result of adding Number fields of
     * provided reading from corresponding fields of this reading. Non-Number
     * fields are set to the values of this reading.
     *
     * @param other reading with values to be added to this reading
     * @return result of adding Number fields of provided reading to
     *      this reading
     */
    public InterfaceReading add(
            final InterfaceReading other) {
        return adjustWith(other, Long::sum);
    }

    /**
     * Creates a reading with values of number fields set to zero. Non-number
     * fields are unchanged from this reading.
     *
     * @return reading with values of number fields set to zero
     */
    public InterfaceReading zero() {
        return substract(this);
    }

    private InterfaceReading adjustWith(
            final InterfaceReading other,
            final BiFunction<Long, Long, Long> adjusterRaw) {
        final BiFunction<Long, Long, Long> adjuster = (long1, long2) -> {
            if (long1 == null || long2 == null) {
                return null;
            }
            return adjusterRaw.apply(long1, long2);
        };

        return new InterfaceReading(
                measurementId,
                interfaceIndex,
                instant,
                adminStatus,
                operStatus,
                adjuster.apply(inBytes, other.inBytes),
                adjuster.apply(inDiscards, other.inDiscards),
                adjuster.apply(inErrors, other.inErrors),
                adjuster.apply(inMulticastPackets, other.inMulticastPackets),
                adjuster.apply(inUnicastPackets, other.inUnicastPackets),
                adjuster.apply(inUnknownProtocols, other.inUnknownProtocols),
                adjuster.apply(outBytes, other.outBytes),
                adjuster.apply(outDiscards, other.outDiscards),
                adjuster.apply(outErrors, other.outErrors),
                adjuster.apply(outMulticastPackets, other.outMulticastPackets),
                adjuster.apply(outUnicastPackets, other.outUnicastPackets));
    }

    public Long getMeasurementId() {
        return measurementId;
    }

    public Integer getInterfaceIndex() {
        return interfaceIndex;
    }

    public Instant getInstant() {
        return instant;
    }

    public AdminStatus getAdminStatus() {
        return adminStatus;
    }

    public OperStatus getOperStatus() {
        return operStatus;
    }

    @VisibleForTesting
    public void setMeasurementId(final Long measurementId) {
        this.measurementId = measurementId;
    }

    /**
     * Adjust in/out bytes with provided adjuster.
     *
     * @param bytesAdjuster function for adjusting bytes of this reading
     * @return InterfaceReading with in/out bytes adjusted
     */
    public InterfaceReading adjustBytes(final Function<Long, Long> bytesAdjuster) {
        return new InterfaceReading(
                measurementId,
                interfaceIndex,
                instant,
                adminStatus,
                operStatus,
                inBytes == null ? null : bytesAdjuster.apply(inBytes),
                inDiscards,
                inErrors,
                inMulticastPackets,
                inUnicastPackets,
                inUnknownProtocols,
                outBytes == null ? null : bytesAdjuster.apply(outBytes),
                outDiscards,
                outErrors,
                outMulticastPackets,
                outUnicastPackets);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("Measurement Id", measurementId)
                .add("Interface Index", interfaceIndex)
                .add("Instant", instant)
                .add("Admin Status", adminStatus)
                .add("Oper Status", operStatus)
                .add("Network Activity", super.toString())
                .toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof InterfaceReading)) {
            return false;
        }
        InterfaceReading otherInterfaceReading = (InterfaceReading) other;
        return super.equals(otherInterfaceReading)
                && Objects.equal(measurementId, otherInterfaceReading.measurementId)
                && Objects.equal(interfaceIndex, otherInterfaceReading.interfaceIndex)
                && Objects.equal(instant, otherInterfaceReading.instant)
                && Objects.equal(adminStatus, otherInterfaceReading.adminStatus)
                && Objects.equal(operStatus, otherInterfaceReading.operStatus);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(
                super.hashCode(),
                measurementId,
                interfaceIndex,
                instant,
                adminStatus,
                operStatus);
    }
}
