package hotspotinternet.restservice.entity;

import static com.google.common.base.Preconditions.checkArgument;

import java.time.Instant;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;

/**
 * Class for storing network activity information of an interface. It
 * defines an interval during which the indicated activity occurred.
 */
public final class InterfaceActivity extends NetworkActivity {

    private int interfaceIndex;
    private Instant startInstant;
    private Instant stopInstant;

    /**
     * Default constructor.
     */
    public InterfaceActivity() {}

    /**
     * Construct InterfaceActivity with provided arguments.
     *
     * @param interfaceIndex index of interface
     * @param startInstant start of interval of activity
     * @param stopInstant end of interval of activity
     * @param networkActivity activity of interface
     */
    public InterfaceActivity(
            final int interfaceIndex,
            final Instant startInstant,
            final Instant stopInstant,
            final NetworkActivity networkActivity) {
        super(
                networkActivity.getInBytes(), networkActivity.inDiscards, networkActivity.inErrors,
                networkActivity.inMulticastPackets, networkActivity.inUnicastPackets,
                networkActivity.inUnknownProtocols, networkActivity.getOutBytes(),
                networkActivity.outDiscards, networkActivity.outErrors,
                networkActivity.outMulticastPackets, networkActivity.outUnicastPackets);
        this.interfaceIndex = interfaceIndex;
        this.startInstant = startInstant;
        this.stopInstant = stopInstant;
    }

    /**
     * Creates a list of InterfaceActivity instances from provided readings. The network
     * activity during the interval between adjacent (chronologically) readings is calculated.
     * Note that the returned list will have a size that is one less than the provided list.
     *
     * @param readings list of readings
     * @return list of InterfaceActivity activity instances representing usage of the readings
     */
    public static List<InterfaceActivity> fromReadings(final List<InterfaceReading> readings) {
        checkArgument(
                readings.size() > 1,
                "Must provide at least 2 readings in order to calculate activity");
        final List<InterfaceReading> sortedReadings = readings
                .stream()
                .sorted(Comparator.comparing(InterfaceReading::getInstant))
                .collect(Collectors.toList());

        final List<InterfaceActivity> interfaceActivities =
                Lists.newArrayListWithExpectedSize(sortedReadings.size() - 1);
        for (int index = 0; index < sortedReadings.size() - 1; ++index) {
            final InterfaceReading currentReading = sortedReadings.get(index);
            final InterfaceReading nextReading = sortedReadings.get(index + 1);
            interfaceActivities.add(new InterfaceActivity(
                    currentReading.getInterfaceIndex(),
                    currentReading.getInstant(),
                    nextReading.getInstant(),
                    nextReading.substract(currentReading)));
        }
        return interfaceActivities;
    }

    public int getInterfaceIndex() {
        return interfaceIndex;
    }

    public Instant getStartInstant() {
        return startInstant;
    }

    public Instant getStopInstant() {
        return stopInstant;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("Interface Index", interfaceIndex)
                .add("Start Instant", startInstant)
                .add("Stop Instant", stopInstant)
                .add("Network Activity", super.toString())
                .toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof InterfaceActivity)) {
            return false;
        }
        InterfaceActivity otherInterfaceUsage = (InterfaceActivity) other;
        return super.equals(otherInterfaceUsage)
                && Objects.equal(interfaceIndex, otherInterfaceUsage.interfaceIndex)
                && Objects.equal(startInstant, otherInterfaceUsage.startInstant)
                && Objects.equal(stopInstant, otherInterfaceUsage.stopInstant);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), interfaceIndex, startInstant, stopInstant);
    }
}
