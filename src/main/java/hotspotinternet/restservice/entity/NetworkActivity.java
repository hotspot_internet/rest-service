package hotspotinternet.restservice.entity;

import javax.persistence.MappedSuperclass;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Class for storing information about network activity.
 */
@MappedSuperclass
public class NetworkActivity {

    protected Long inBytes;
    protected Long inDiscards;
    protected Long inErrors;
    protected Long inMulticastPackets;
    protected Long inUnicastPackets;
    protected Long inUnknownProtocols;
    protected Long outDiscards;
    protected Long outErrors;
    protected Long outMulticastPackets;
    protected Long outUnicastPackets;
    protected Long outBytes;

    /**
     * Default constructor.
     */
    public NetworkActivity() {}

    /**
     * Construct NetworkActivity with provided arguments.
     *
     * @param inBytes number of bytes received
     * @param inDiscards in-packets discarded
     * @param inErrors in-packet errors
     * @param inMulticastPackets multicast packets received
     * @param inUnicastPackets unicast packets received
     * @param inUnknownProtocols packets received with unknown protocol
     * @param outBytes number of bytes sent out
     * @param outDiscards out-packets discarded
     * @param outErrors out-packet errors
     * @param outMulticastPackets multicast packets sent
     * @param outUnicastPackets unicast packets sent
     */
    protected NetworkActivity(
            final Long inBytes,
            final Long inDiscards,
            final Long inErrors,
            final Long inMulticastPackets,
            final Long inUnicastPackets,
            final Long inUnknownProtocols,
            final Long outBytes,
            final Long outDiscards,
            final Long outErrors,
            final Long outMulticastPackets,
            final Long outUnicastPackets) {
        this.inBytes = inBytes;
        this.inDiscards = inDiscards;
        this.inErrors = inErrors;
        this.inMulticastPackets = inMulticastPackets;
        this.inUnicastPackets = inUnicastPackets;
        this.inUnknownProtocols = inUnknownProtocols;
        this.outBytes = outBytes;
        this.outDiscards = outDiscards;
        this.outErrors = outErrors;
        this.outMulticastPackets = outMulticastPackets;
        this.outUnicastPackets = outUnicastPackets;
    }

    public Long getInBytes() {
        return inBytes;
    }

    public Long getInDiscards() {
        return inDiscards;
    }

    public Long getInErrors() {
        return inErrors;
    }

    public Long getInMulticastPackets() {
        return inMulticastPackets;
    }

    public Long getInUnicastPackets() {
        return inUnicastPackets;
    }

    public Long getInUnknownProtocols() {
        return inUnknownProtocols;
    }

    public Long getOutBytes() {
        return outBytes;
    }

    public Long getOutDiscards() {
        return outDiscards;
    }

    public Long getOutErrors() {
        return outErrors;
    }

    public Long getOutMulticastPackets() {
        return outMulticastPackets;
    }

    public Long getOutUnicastPackets() {
        return outUnicastPackets;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("In Bytes", inBytes)
                .add("In Discards", inDiscards)
                .add("In Errors", inErrors)
                .add("In Multicast Packets", inMulticastPackets)
                .add("In Unicast Packets", inUnicastPackets)
                .add("In Unknown Protocols", inUnknownProtocols)
                .add("Out Bytes", outBytes)
                .add("Out Discards", outDiscards)
                .add("Out Errors", outErrors)
                .add("Out Multicast Packets", outMulticastPackets)
                .add("Out Unicast Packets", outUnicastPackets)
                .toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof NetworkActivity)) {
            return false;
        }
        NetworkActivity otherInterfaceReading = (NetworkActivity) other;
        return Objects.equal(inBytes, otherInterfaceReading.inBytes)
                && Objects.equal(inDiscards, otherInterfaceReading.inDiscards)
                && Objects.equal(inErrors, otherInterfaceReading.inErrors)
                && Objects.equal(inMulticastPackets, otherInterfaceReading.inMulticastPackets)
                && Objects.equal(inUnicastPackets, otherInterfaceReading.inUnicastPackets)
                && Objects.equal(inUnknownProtocols, otherInterfaceReading.inUnknownProtocols)
                && Objects.equal(outBytes, otherInterfaceReading.outBytes)
                && Objects.equal(outDiscards, otherInterfaceReading.outDiscards)
                && Objects.equal(outErrors, otherInterfaceReading.outErrors)
                && Objects.equal(outMulticastPackets, otherInterfaceReading.outMulticastPackets)
                && Objects.equal(outUnicastPackets, otherInterfaceReading.outUnicastPackets);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(
                inBytes,
                inDiscards,
                inErrors,
                inMulticastPackets,
                inUnicastPackets,
                inUnknownProtocols,
                outBytes,
                outDiscards,
                outErrors,
                outMulticastPackets,
                outUnicastPackets);
    }
}
