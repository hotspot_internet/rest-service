package hotspotinternet.restservice.entity;

import java.util.Arrays;

/**
 * Enumerations for column ifOperStatus of SNMP IF-MIB 'interfaces' table.
 */
public enum OperStatus {
    /** UP. */
    UP(1),
    /** DOWN. */
    DOWN(2),
    /** TESTING. */
    TESTING(3),
    /** UNKNOWN. */
    UNKNOWN(4),
    /** DORMANT. */
    DORMANT(5),
    /** NOT_PRESENT. */
    NOT_PRESENT(6),
    /** LOWER_LAYER_DOWN. */
    LOWER_LAYER_DOWN(7);

    private final int snmpValue;

    private OperStatus(final int snmpValue) {
        this.snmpValue = snmpValue;
    }

    /**
     * Gets OperStatus corresponding to provided SNMP value.
     *
     * @param snmpValue integer corresponding to SNMP OperStatus
     * @return OperStatus corresponding to provided SNMP value
     * @throws IllegalArgumentException if provided snmpValue does not map to an OperStatus
     */
    public static OperStatus fromSnmpValue(final int snmpValue) {
        return Arrays.asList(values())
                .stream()
                .filter(operStatus -> operStatus.snmpValue == snmpValue)
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException(String.format(
                        "Received SNMP value of %s does not map to an OperStatus",
                        snmpValue)));
    }
}
