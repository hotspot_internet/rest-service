package hotspotinternet.restservice.entity;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

/**
 * Repository for storing/retrieving instances of DataLimit.
 */
public interface DataLimitRepository extends CrudRepository<DataLimit, Long> {
    List<DataLimit> findByInterfaceIndex(Integer interfaceIndex);
}
