package hotspotinternet.restservice.entity;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Optional;

/**
 * Class for storing information of a data overage.
 */
@Entity
public final class Overage {
    private static double HOUR_THRESHOLD_PERCENT = 1;
    private static long OVERAGE_THRESHOLD_BYTES = (long) 6e9;

    /**
     * Type of data overage.
     */
    public enum OverageType {
        /** An overage based on data usage within an hour. */
        HOUR(ChronoUnit.HOURS),
        /** An overage based on data usage relative to total cycle limit. */
        CYCLE(ChronoUnit.DAYS);

        private final ChronoUnit chronoUnit;

        private OverageType(final ChronoUnit chronoUnit) {
            this.chronoUnit = chronoUnit;
        }

        /**
         * Gets the minimum interval between consecutive notifications.
         *
         * @return minimum interval between consecutive notifications
         */
        public ChronoUnit getNotificationInterval() {
            return chronoUnit;
        }
    }

    private @Id @GeneratedValue Long overageId;
    private OverageType overageType;
    private Instant instant;

    @Transient private String description;

    // Empty, no-arg constructor for JSON mapping
    public Overage() { }

    /**
     * Constructs Overage from provided parameters.
     *
     * @param description of overage
     * @param overageType type of overage
     * @param instant at which overage calculate was performed
     */
    public Overage(
            final String description,
            final OverageType overageType,
            final Instant instant) {
        this.description = checkNotNull(description);
        this.overageType = checkNotNull(overageType);
        this.instant = checkNotNull(instant);
    }

    private static double bytesToGb(long bytes) {
        return new BigDecimal(bytes).divide(new BigDecimal((long) 1e9)).doubleValue();
    }

    private static String createCycleOverageDescription(
            final long usedBytes,
            final long expectedUsedBytes) {
        return String.format(
                "Cycle usage of %.1f GB exceeds allowed usage "
                        + "of %.1f GB by more than buffer of %.1f GB",
                bytesToGb(usedBytes),
                bytesToGb(expectedUsedBytes),
                bytesToGb(OVERAGE_THRESHOLD_BYTES));
    }

    private static String createHourOverageDescription(final long usedBytes,
            final long hourAllowedBytes) {
        return String.format(
                "Hour usage of %.2f GB exceeds allowed usage "
                        + "of %.2f GB (%s%% of total cycle limit)",
                bytesToGb(usedBytes),
                bytesToGb((long) hourAllowedBytes),
                HOUR_THRESHOLD_PERCENT);
    }

    /**
     * Create hour-based Overage if overage is beyond threshold.
     *
     * @param instant at which overage calculation occurs
     * @param hourUsedBytes number of bytes used from start of hour to calculation instant
     * @param cycleAllowedBytes number of bytes allowed in the data cycle
     * @return Overage if overage is beyond threshold, otherwise absent
     */
    public static Optional<Overage> createHourOverage(final Instant instant,
            final long hourUsedBytes,
            final long cycleAllowedBytes) {
        double allowedHourBytes = cycleAllowedBytes * HOUR_THRESHOLD_PERCENT / 100;
        if (hourUsedBytes < allowedHourBytes) {
            return Optional.absent();
        }
        return Optional.of(new Overage(
                createHourOverageDescription(
                        hourUsedBytes,
                        (long) allowedHourBytes),
                OverageType.HOUR,
                instant));
    }

    /**
     * Create cycle-based Overage if overage is beyond threshold.
     *
     * @param instant at which overage calculation occurs
     * @param usedBytes number of bytes used from start of data cycle to instant
     * @param expectedUsedBytes number of bytes expected to be be used by end of
     *      day in order for all of the data to be used at the end of the cycle
     * @return Overage if overage is beyond threshold, otherwise absent
     */
    public static Optional<Overage> createCycleOverage(final Instant instant,
            final long usedBytes,
            final long expectedUsedBytes) {
        long overageBytes = usedBytes - expectedUsedBytes;
        if (overageBytes < OVERAGE_THRESHOLD_BYTES) {
            return Optional.absent();
        }
        return Optional.of(new Overage(
                createCycleOverageDescription(usedBytes, expectedUsedBytes),
                OverageType.CYCLE,
                instant));
    }

    public String getDescription() {
        return description;
    }

    public Long getOverageId() {
        return overageId;
    }

    public OverageType getOverageType() {
        return overageType;
    }


    public Instant getInstant() {
        return instant;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("Overage ID", overageId)
                .add("Description", description)
                .add("Instant", instant)
                .add("Overage Type", overageType)
                .toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Overage)) {
            return false;
        }
        Overage otherOverage = (Overage) other;

        return Objects.equal(overageId, otherOverage.overageId)
                && Objects.equal(description, otherOverage.description)
                && Objects.equal(instant, otherOverage.instant)
                && Objects.equal(overageType, otherOverage.overageType);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(
                overageId,
                description,
                instant,
                overageType);
    }
}
