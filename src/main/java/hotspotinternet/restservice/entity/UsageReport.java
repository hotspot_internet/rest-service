package hotspotinternet.restservice.entity;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.time.Instant;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Class for storing information of a data usage report. It describes an instant
 * at which the data usage is known.
 */
@Entity
public class UsageReport {

    protected @Id @GeneratedValue Long usageReportId;
    protected String messageId;
    protected String source;
    protected Instant instant;
    protected Long usedBytes;
    protected String phoneNumber;
    protected Double usedPercent;

    /**
     * No-arg empty constructor to allow marshalling to/from JSON.
     */
    public UsageReport() { }

    /**
     * Constructs UsageReport from provided arguments. 1 and only one of
     * usedPercent and usedBytes must be non-null.
     */
    @VisibleForTesting
    public UsageReport(
            final Long usageReportId,
            final String messageId,
            final String source,
            final Instant instant,
            final Long usedBytes,
            final String phoneNumber,
            final Double usedPercent) {
        checkArgument(
                usedBytes == null ^ usedPercent == null,
                "1 and only 1 of usedPercent and usedBytes must be non-null");
        this.messageId = checkNotNull(messageId);
        this.source = checkNotNull(source);
        this.instant = checkNotNull(instant);
        this.phoneNumber = checkNotNull(phoneNumber);
        this.usageReportId = usageReportId;
        this.usedPercent = usedPercent;
        this.usedBytes = usedBytes;
    }

    public String getSource() {
        return source;
    }

    public String getMessageId() {
        return messageId;
    }

    public Double getUsedPercent() {
        return usedPercent;
    }

    public Long getUsedBytes() {
        return usedBytes;
    }

    public Instant getInstant() {
        return instant;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Long getUsageReportId() {
        return usageReportId;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("Report ID", usageReportId)
                .add("Message ID", messageId)
                .add("Source", source)
                .add("Used Bytes", usedBytes)
                .add("Used Percent", usedPercent)
                .add("Phone Number", phoneNumber)
                .add("Instant", instant)
                .toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof UsageReport)) {
            return false;
        }
        UsageReport otherUsageReport = (UsageReport) other;
        return Objects.equal(usageReportId, otherUsageReport.usageReportId)
                && Objects.equal(messageId, otherUsageReport.messageId)
                && Objects.equal(source, otherUsageReport.source)
                && Objects.equal(usedBytes, otherUsageReport.usedBytes)
                && Objects.equal(usedPercent, otherUsageReport.usedPercent)
                && Objects.equal(phoneNumber, otherUsageReport.phoneNumber)
                && Objects.equal(instant, otherUsageReport.instant);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(
                usageReportId,
                messageId,
                source,
                usedBytes,
                usedPercent,
                phoneNumber,
                instant);
    }
}
