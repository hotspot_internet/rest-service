package hotspotinternet.restservice.entity;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

/**
 * Repository for storing/retrieving instances of InterfaceReading.
 */
public interface InterfaceReadingRepository extends CrudRepository<InterfaceReading, Long> {
    List<InterfaceReading> findByInterfaceIndexAndInstantBetween(
            Integer interfaceIndex,
            Instant fromInstant,
            Instant toInstant);

    Optional<InterfaceReading> findFirstByInterfaceIndexOrderByInstantDesc(Integer interfaceIndex);
}
