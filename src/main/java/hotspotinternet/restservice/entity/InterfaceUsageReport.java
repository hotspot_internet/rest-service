package hotspotinternet.restservice.entity;

import static com.google.common.base.Preconditions.checkNotNull;

import java.math.BigDecimal;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

/**
 * Class for storing information of a data usage report. It describes an instant
 * at which the data usage is known.
 */
public class InterfaceUsageReport extends UsageReport {
    private int interfaceIndex;

    /**
     * No-arg empty constructor to allow marshalling to/from JSON.
     */
    public InterfaceUsageReport() { }

    /**
     * Constructs InterfaceUsageReport from provided arguments.
     */
    @VisibleForTesting
    public InterfaceUsageReport(
            final int interfaceIndex,
            final UsageReport usageReport,
            final DataLimit dataLimit) {
        this.interfaceIndex = interfaceIndex;
        this.messageId = checkNotNull(usageReport.getMessageId());
        this.source = checkNotNull(usageReport.getSource());
        this.instant = checkNotNull(usageReport.getInstant());
        this.phoneNumber = checkNotNull(usageReport.getPhoneNumber());
        this.usageReportId = usageReport.getUsageReportId();

        if (usageReport.getUsedPercent() != null) {
            this.usedBytes =
                    new BigDecimal(usageReport.getUsedPercent() / 100)
                    .multiply(new BigDecimal(dataLimit.getBytes()))
                    .longValue();
            this.usedPercent = usageReport.getUsedPercent();
        } else {
            this.usedBytes = usageReport.getUsedBytes();
            this.usedPercent =
                    new BigDecimal(usageReport.getUsedBytes())
                    .divide(new BigDecimal(dataLimit.getBytes()))
                    .doubleValue()
                    * 100;
        }
    }

    public int getInterfaceIndex() {
        return interfaceIndex;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("Usage Report", super.toString())
                .add("Interface Index", interfaceIndex)
                .toString();
    }

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof InterfaceUsageReport)) {
            return false;
        }
        InterfaceUsageReport otherUsageReport = (InterfaceUsageReport) other;
        return super.equals(otherUsageReport)
                && interfaceIndex == otherUsageReport.interfaceIndex;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(interfaceIndex, super.hashCode());
    }
}
