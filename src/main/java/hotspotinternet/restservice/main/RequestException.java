package hotspotinternet.restservice.main;

/**
 * General exception for any HTTP request.
 */
public class RequestException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    /**
     * Constructs exception with provided message.
     *
     * @param message describing exception
     */
    RequestException(final String message) {
        super(message);
    }
}
