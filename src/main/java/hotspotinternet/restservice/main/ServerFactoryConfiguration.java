package hotspotinternet.restservice.main;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Additional configuration bean(s) for REST application. Declaring these beans
 * separate from the @SpringBootApplication class delays initializing of these
 * bean(s) until after dependent beans are initialized.
 */
@Configuration
public class ServerFactoryConfiguration {
    @Value("${server.http.port}")
    private int httpPort;

    /**
     * Creates web server factory.
     *
     * @return web server factory
     */
    @Bean
    public ServletWebServerFactory webServerFactory() {
        TomcatServletWebServerFactory tomcatServerFactory = new TomcatServletWebServerFactory();
        tomcatServerFactory.addAdditionalTomcatConnectors(createHttpConnector(httpPort));
        return tomcatServerFactory;
    }

    private static Connector createHttpConnector(final int port) {
        final Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
        final Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
        connector.setScheme("http");
        connector.setSecure(false);
        connector.setPort(port);
        protocol.setSSLEnabled(false);
        return connector;
    }
}
