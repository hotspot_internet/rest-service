package hotspotinternet.restservice.main;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;

import hotspotinternet.restservice.entity.DataLimit;
import hotspotinternet.restservice.entity.DataLimitRepository;
import hotspotinternet.restservice.entity.InterfaceActivity;
import hotspotinternet.restservice.entity.InterfaceUsageReport;
import hotspotinternet.restservice.entity.Overage;
import hotspotinternet.restservice.entity.Overage.OverageType;
import hotspotinternet.restservice.entity.OverageRepository;
import hotspotinternet.restservice.entity.UsageReport;
import hotspotinternet.restservice.entity.UsageReportRepository;
import hotspotinternet.restservice.interval.ReadingInterval;

/**
 * Controls/handles requests to notifications URL ('/notifications/...').
 */
@CrossOrigin
@Controller
public class NotificationsController {
    private static final Logger LOG = LoggerFactory.getLogger(NotificationsController.class);
    private static final ImmutableMap<Integer, String> IFACE_INDEX_TO_PHONE_NUMBER_MAP =
            ImmutableMap.of(
                    1, "0529",
                    2, "2999",
                    3, "2600");

    @Value("${server.ssl.key-alias}")
    private String restHost;
    @Value("${server.http.port}")
    private int httpPort;
    @Autowired
    private DataLimitRepository dataLimitRepo;
    @Autowired
    private OverageRepository overageRepo;
    @Autowired
    private UsageReportRepository usageReportRepo;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private InstantService instantService;

    /**
     * Handles POST requests to '/notifications/usagereport/' URL.
     *
     * @return ResponseEntity with written UsageReport, or existing report
     *      if report already exists in data base
     */
    @PostMapping("/notifications/usagereport")
    public @ResponseBody ResponseEntity<UsageReport> postUsageReport(
            @RequestBody UsageReport usageReport) {
        if (!(usageReport.getUsedBytes() == null ^ usageReport.getUsedPercent() == null)) {
            throw new RequestException("1 and only 1 of usedBytes and usedPercent should be null");
        }
        java.util.Optional<UsageReport> existingUsageReport = usageReportRepo
                .findFirstByMessageId(usageReport.getMessageId());
        if (existingUsageReport.isPresent()) {
            return ResponseEntity
                    .status(HttpStatus.ALREADY_REPORTED)
                    .body(existingUsageReport.get());
        }
        return ResponseEntity.ok(usageReportRepo.save(usageReport));
    }

    /**
     * Handles GET requests to '/notifications/overage' URL.
     */
    @GetMapping("/notifications/overage")
    public @ResponseBody List<Overage> getDayBasedOverage(
            @RequestParam(value = "start", required = false) Instant startParam,
            @RequestParam(value = "stop", required = false) Instant stopParam) {
        if ((startParam == null) != (stopParam == null)) {
            throw new RequestException(
                    "'start' and 'stop' params should both be either present or absent");
        }
        if (startParam != null && stopParam != null) {
            return overageRepo.findByInstantBetween(startParam, stopParam);
        }

        final List<DataLimit> dataLimits = IFACE_INDEX_TO_PHONE_NUMBER_MAP.keySet()
                .stream()
                .map(this::getEarliestDataLimit)
                .collect(Collectors.toList());
        if (dataLimits.isEmpty()) {
            throw new RequestException("No data limits present");
        }

        long cycleBytes = dataLimits
                .stream()
                .map(DataLimit::getBytes)
                .reduce(0L, Long::sum);

        Optional<Overage> cycleOverage = getCycleOverage(dataLimits.get(0), cycleBytes);
        boolean existsCycleOverage = cycleOverage.isPresent();
        cycleOverage = !existsRecentOverage(OverageType.CYCLE) ? cycleOverage : Optional.absent();
        Optional<Overage> hourOverage =
                existsCycleOverage && !existsRecentOverage(OverageType.HOUR)
                ? getHourOverage(cycleBytes)
                : Optional.absent();
        return Arrays.asList(cycleOverage, hourOverage)
                .stream()
                .filter(Optional::isPresent)
                .map(Optional::get)
                .map(overageRepo::save)
                .collect(Collectors.toList());
    }

    private boolean existsRecentOverage(final OverageType overageType) {
        final Instant nowInstant = instantService.now();
        return !overageRepo
                .findByOverageTypeAndInstantBetween(
                        overageType,
                        nowInstant
                                .atOffset(ZoneOffset.UTC)
                                .minus(1, overageType.getNotificationInterval())
                                .toInstant(),
                        nowInstant)
                .isEmpty();
    }

    private Optional<Overage> getCycleOverage(DataLimit dataLimit, long cycleBytes) {
        final Instant nowInstant = instantService.now();
        final Instant cycleStartInstant = dataLimit.getStartInstant(nowInstant).get();
        final Instant cycleStopInstant = dataLimit.getStopInstant(nowInstant).get();

        final Instant overageCutoffInstant = cycleStopInstant.minus(2, ChronoUnit.DAYS);
        if (nowInstant.isAfter(overageCutoffInstant)) {
            return Optional.absent();
        }

        long usedBytes = IFACE_INDEX_TO_PHONE_NUMBER_MAP.keySet()
                .stream()
                .map(interfaceIndex ->
                        getUsage(interfaceIndex, cycleStartInstant, ReadingInterval.MONTH))
                .reduce(0L, Long::sum);
        OffsetDateTime nowDateTime = nowInstant.atOffset(ZoneOffset.UTC);
        final Instant endOfDayInstant = cycleStartInstant
                .atOffset(ZoneOffset.UTC)
                .withDayOfYear(nowDateTime.getDayOfYear())
                .withYear(nowDateTime.getYear())
                .plus(1, ChronoUnit.DAYS)
                .toInstant();
        long cycleMillis = Duration.between(cycleStartInstant, cycleStopInstant).toMillis();
        long dayEndElapsedMillis = Duration.between(cycleStartInstant, endOfDayInstant).toMillis();
        long expectedUsedBytes = BigDecimal.valueOf(dayEndElapsedMillis)
                .divide(BigDecimal.valueOf(cycleMillis), new MathContext(10, RoundingMode.HALF_UP))
                .multiply((BigDecimal.valueOf(cycleBytes)))
                .longValue();
        return Overage.createCycleOverage(nowInstant, usedBytes, expectedUsedBytes);
    }

    private Optional<Overage> getHourOverage(final long cycleAllowedBytes) {
        final Instant nowInstant = instantService.now();
        OffsetDateTime nowDateTime = nowInstant.atOffset(ZoneOffset.UTC);
        Instant hourStartInstant = Instant.EPOCH
                .atOffset(ZoneOffset.UTC)
                .withDayOfYear(nowDateTime.getDayOfYear())
                .withYear(nowDateTime.getYear())
                .withHour(nowDateTime.getHour())
                .toInstant();
        long hourUsedBytes = IFACE_INDEX_TO_PHONE_NUMBER_MAP.keySet()
                .stream()
                .map(interfaceIndex ->
                        getUsage(interfaceIndex, hourStartInstant, ReadingInterval.HOUR))
                .reduce(0L, Long::sum);
        return Overage.createHourOverage(nowInstant, hourUsedBytes, cycleAllowedBytes);
    }

    @SuppressWarnings("unchecked")
    private long getUsage(final int interfaceIndex,
            final Instant cycleStartInstant,
            final ReadingInterval period) {
        final String url = String.format(
                "http://%s:%s/activity/interface/1?start=%s&period=%s",
                restHost,
                httpPort,
                cycleStartInstant,
                period);
        ActivityCollectionModel collectionModel =
                restTemplate.getForObject(url, ActivityCollectionModel.class);
        if (collectionModel == null) {
            LOG.error("null returned when getting interface activities");
            throw new RequestException("Error retrieving interface activities");
        }
        Iterator<EntityModel<InterfaceActivity>> activityIterator = collectionModel
                .getContent()
                .iterator();
        if (!activityIterator.hasNext()) {
            throw new RequestException("No activities received");
        }

        InterfaceActivity interfaceActivity = activityIterator.next().getContent();
        if (interfaceActivity == null) {
            LOG.error("null returned when getting content of interface activity model");
            throw new RequestException("Error retrieving interface activities");
        }
        return interfaceActivity.getInBytes() + interfaceActivity.getOutBytes();
    }

    /**
     * Handles GET requests to '/notifications/usagereport/interface/{interfaceIndex}' URL.
     *
     * @param interfaceIndex for which to get usage reports
     * @param instantParam instant for which to get active usage reports
     * @return list of InterfaceUsageReports relevant to provided parameters
     */
    @GetMapping("/notifications/usagereport/interface/{interfaceIndex}")
    public @ResponseBody List<InterfaceUsageReport> getInterfaceUsageReports(
            @PathVariable Integer interfaceIndex,
            @RequestParam(value = "instant", required = false) Instant instantParam) {
        final Instant instant = instantParam == null
                ? Instant.now()
                : instantParam;
        List<DataLimit> dataLimits = dataLimitRepo
                .findByInterfaceIndex(interfaceIndex)
                .stream()
                .filter(dataLimit -> dataLimit.isFor(instant))
                .collect(Collectors.toList());

        if (dataLimits.isEmpty()) {
            throw new RequestException(String.format(
                    "No data limits available for interface %s instant of '%s'",
                    interfaceIndex,
                    instant));
        } else if (dataLimits.size() > 1) {
            LOG.warn(
                    "Multiple relevant data limits found for interface '{}' - using first",
                    interfaceIndex);
        }
        DataLimit dataLimit = dataLimits.get(0);
        Instant cycleStart = dataLimit.getStartInstant(instant).get();
        Instant cycleStop = dataLimit.getStopInstant(instant).get();

        final String interfacePhoneNumber = IFACE_INDEX_TO_PHONE_NUMBER_MAP.get(interfaceIndex);
        if (interfacePhoneNumber == null) {
            throw new RequestException(String.format(
                    "Phone number for interface %s is not known",
                    interfaceIndex));
        }

        return usageReportRepo
                .findByPhoneNumberContaining(interfacePhoneNumber)
                .stream()
                .filter(usageReport -> usageReport.getInstant().isAfter(cycleStart))
                .filter(usageReport -> usageReport.getInstant().isBefore(cycleStop))
                .map(usageReport ->
                        new InterfaceUsageReport(interfaceIndex, usageReport, dataLimit))
                .collect(Collectors.toList());
    }

    private DataLimit getEarliestDataLimit(final int interfaceIndex) {
        final Instant nowInstant = instantService.now();
        return dataLimitRepo
                .findByInterfaceIndex(interfaceIndex)
                .stream()
                .filter(dataLimit -> dataLimit.getStartInstant(nowInstant).isPresent())
                .sorted(Comparator.comparing(
                        dataLimit -> dataLimit.getStartInstant(nowInstant).get()))
                .findFirst()
                .orElseThrow(() ->
                        new RequestException("No data limit relevant to instant: " + nowInstant));
    }
}
