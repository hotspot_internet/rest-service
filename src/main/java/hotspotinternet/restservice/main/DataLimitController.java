package hotspotinternet.restservice.main;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import hotspotinternet.restservice.entity.DataLimit;
import hotspotinternet.restservice.entity.DataLimitRepository;

/**
 * Controls/handles requests to data limit URL ('/datalimit/interface/').
 */
@CrossOrigin
@Controller
public class DataLimitController {
    @Autowired
    private DataLimitRepository dataLimitRepository;

    /**
     * Handles requests to '/datalimit/interface/{interfaceIndex}' URL.
     *
     * @return DataLimit with written data limit
     */
    @PostMapping("/datalimit/interface/{interfaceIndex}")
    public @ResponseBody DataLimit postDataLimit(
            @PathVariable Integer interfaceIndex,
            @RequestBody DataLimit dataLimit) {
        return dataLimitRepository.save(dataLimit);
    }

    /**
     * Handles GET requests to '/datalimit/interface/{interfaceIndex}' URL.
     *
     * @param interfaceIndex for which to get data limits
     * @param isAll whether to return all data limits or only active limits
     * @param instantParam instant for which active data limits should be returned
     * @return list of DataLimits relevant to provided parameters
     */
    @GetMapping("/datalimit/interface/{interfaceIndex}")
    public @ResponseBody List<DataLimit> getDataLimits(
            @PathVariable Integer interfaceIndex,
            @RequestParam(value = "all", defaultValue = "false") boolean isAll,
            @RequestParam(value = "instant", required = false) Instant instantParam) {
        final Instant instant = instantParam == null ? Instant.now() : instantParam;
        return dataLimitRepository.findByInterfaceIndex(interfaceIndex)
                .stream()
                .filter(dataLimit -> isAll ? true : dataLimit.isFor(instant))
                .collect(Collectors.toList());
    }
}
