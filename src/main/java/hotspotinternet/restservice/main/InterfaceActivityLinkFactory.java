package hotspotinternet.restservice.main;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.time.Instant;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.Link;

import com.google.common.base.Optional;

import hotspotinternet.restservice.entity.InterfaceActivity;
import hotspotinternet.restservice.interval.ActivityPeriods;
import hotspotinternet.restservice.interval.ReadingInterval;

/**
 * Creates links for a CollectionModel of InterfaceActivity instances.
 */
public class InterfaceActivityLinkFactory {
    private final int interfaceIndex;
    private final List<ActivityPeriods> topLevelPeriodsList;
    private final boolean isDivided;

    /**
     * Constructs InterfaceActivityLinkFactory from provided arguments.
     *
     * @param interfaceIndex of activities
     * @param topLevelPeriodsList List of ActivityPeriods for the original periods (i.e. not
     *      divided into shorter-duration periods) of the request URL
     * @param isDivided that is true if each period(s) should be divided into
     *      shorter-duration periods. For example, divide an hour into 60 minutes
     */
    InterfaceActivityLinkFactory(final int interfaceIndex,
            final List<ActivityPeriods> topLevelPeriodsList,
            final boolean isDivided) {
        this.interfaceIndex = interfaceIndex;
        this.topLevelPeriodsList = topLevelPeriodsList;
        this.isDivided = isDivided;
    }

    /**
     * Creates a link for the getting previous collection model of activities.
     *
     * @return link for getting previous collection model of activities
     */
    Link collectionPreviousLink() {
        final ReadingInterval periodsDurationInterval = topLevelPeriodsList.get(0).getDuration();
        return linkTo(
                methodOn(InterfacesController.class).getActivityCollectionModel(
                        interfaceIndex,
                        isDivided,
                        getStartInstant()
                                .atOffset(ZoneOffset.UTC)
                                .minus(
                                        getActivitiesQuantity(),
                                        periodsDurationInterval.getChronoUnit())
                                .toInstant(),
                        periodsDurationInterval,
                        getActivitiesQuantity())).withRel(IanaLinkRelations.PREV);
    }

    private int getActivitiesQuantity() {
        return topLevelPeriodsList.size();
    }

    private Instant getStartInstant() {
        return topLevelPeriodsList.get(0).getStart();
    }

    /**
     * Creates a self referencing link for the entire CollectionModel.
     *
     * @return link for the CollectionModel to reference itself
     */
    Link collectionSelfLink() {
        return linkTo(methodOn(InterfacesController.class).getActivityCollectionModel(
                interfaceIndex,
                isDivided,
                getStartInstant(),
                topLevelPeriodsList.get(0).getDuration(),
                getActivitiesQuantity())).withSelfRel();
    }

    /**
     * Creates a link for the getting next collection model of activities.
     *
     * @return link for getting next collection model of activities
     */
    Link collectionNextLink() {
        final ReadingInterval periodsDurationInterval = topLevelPeriodsList.get(0).getDuration();
        return linkTo(
                methodOn(InterfacesController.class).getActivityCollectionModel(
                        interfaceIndex,
                        isDivided,
                        getStartInstant()
                                .atOffset(ZoneOffset.UTC)
                                .plus(
                                        getActivitiesQuantity(),
                                        periodsDurationInterval.getChronoUnit())
                                .toInstant(),
                        periodsDurationInterval,
                        getActivitiesQuantity())).withRel(IanaLinkRelations.NEXT);
    }

    /**
     * Creates link for the entire collection model to reference the URL for obtaining
     * longer-duration interface activities that encompass these activity periods.
     *
     * @return link for obtaining interface activities that encompass these activity periods
     */
    Optional<Link> collectionIntervalInLink() {
        ActivityPeriods firstTopLevelUpPeriods = topLevelPeriodsList.get(0).getUpPeriods().orNull();
        if (firstTopLevelUpPeriods == null) {
            return Optional.absent();
        }
        // collect as set since we are not interested in duplicates
        Set<ActivityPeriods> topLevelUpPeriods = topLevelPeriodsList.stream()
                .map(ActivityPeriods::getUpPeriods)
                .map(Optional::get)
                .collect(Collectors.toSet());
        return Optional.of(linkTo(methodOn(InterfacesController.class).getActivityCollectionModel(
                    interfaceIndex,
                    true,
                    firstTopLevelUpPeriods.getStart(),
                    firstTopLevelUpPeriods.getDuration(),
                    topLevelUpPeriods.size())).withRel(IanaLinkRelations.INTERVAL_IN));
    }

    /**
     * Creates link for the entire collection model to reference the URL for obtaining
     * shorter-duration interface activities that in total span the same duration
     * as these activity periods.
     *
     * @return link for obtaining shorter-duration interface activities spanning same duration
     */
    Optional<Link> collectionIntervalContainsLink() {
        List<ActivityPeriods> firstTopLevelDownPeriods =
                topLevelPeriodsList.get(0).getDownPeriods().orNull();
        if (firstTopLevelDownPeriods == null) {
            return Optional.absent();
        }

        return Optional.of(linkTo(methodOn(InterfacesController.class).getActivityCollectionModel(
                    interfaceIndex,
                    true,
                    getStartInstant(),
                    topLevelPeriodsList.get(0).getDuration(),
                    topLevelPeriodsList.size())).withRel(IanaLinkRelations.INTERVAL_CONTAINS));
    }

    Link activityPreviousLink(final ActivityPeriods previousPeriods) {
        return linkTo(
                methodOn(InterfacesController.class).getActivityCollectionModel(
                        interfaceIndex,
                        false,
                        previousPeriods.getStart(),
                        previousPeriods.getDuration(),
                        1)).withRel(IanaLinkRelations.PREV);
    }

    Link activitySelfLink(
            final InterfaceActivity activity,
            final ReadingInterval activityDuration) {
        return linkTo(methodOn(InterfacesController.class).getActivityCollectionModel(
                activity.getInterfaceIndex(),
                false,
                activity.getStartInstant(),
                activityDuration,
                1)).withSelfRel();
    }

    Link activityNextLink(final ActivityPeriods nextPeriods) {
        return linkTo(
                methodOn(InterfacesController.class).getActivityCollectionModel(
                        interfaceIndex,
                        false,
                        nextPeriods.getStart(),
                        nextPeriods.getDuration(),
                        1)).withRel(IanaLinkRelations.NEXT);
    }

    Optional<Link> activityIntervalInLink(final ActivityPeriods activityPeriods) {
        ActivityPeriods upPeriods = activityPeriods.getUpPeriods().orNull();
        if (upPeriods == null) {
            return Optional.absent();
        }

        return Optional.of(linkTo(
                methodOn(InterfacesController.class).getActivityCollectionModel(
                        interfaceIndex,
                        true,
                        upPeriods.getStart(),
                        upPeriods.getDuration(),
                        1)).withRel(IanaLinkRelations.INTERVAL_IN));
    }

    Optional<Link> activityIntervalContainsLink(final ActivityPeriods activityPeriods) {
        List<ActivityPeriods> downPeriods = activityPeriods.getDownPeriods().orNull();
        if (downPeriods == null) {
            return Optional.absent();
        }

        return Optional.of(linkTo(
                methodOn(InterfacesController.class).getActivityCollectionModel(
                        interfaceIndex,
                        true,
                        activityPeriods.getStart(),
                        activityPeriods.getDuration(),
                        1)).withRel(IanaLinkRelations.INTERVAL_CONTAINS));
    }
}
