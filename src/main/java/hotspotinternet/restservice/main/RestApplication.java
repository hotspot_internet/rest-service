package hotspotinternet.restservice.main;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.client.RestTemplate;

/**
 * Main runner for application.
 */
@SpringBootApplication
@EnableWebSecurity
@EnableJpaRepositories(basePackages = "hotspotinternet.restservice.entity")
@EntityScan(basePackages = "hotspotinternet.restservice.entity")
public class RestApplication extends WebSecurityConfigurerAdapter {

    /**
     * Main method.
     *
     * @param args for running
     */
    public static void main(String[] args) {
        SpringApplication.run(RestApplication.class, args);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // allow all GET requests
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET)
                .permitAll();
        // allow any request with valid certificate (null -> any method)
        HttpMethod anyMethod = null;
        http.authorizeRequests()
                .antMatchers(anyMethod)
                .authenticated()
                .and()
                .x509()
                .subjectPrincipalRegex("CN=(.*?)(?:,|$)")
                .userDetailsService(userDetailsService())
                .and()
                .csrf()
                .disable();
    }

    /**
     * Gets UserDetailsService.
     *
     * @return UserDetailsService
     */
    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String username) {
                return new User(username, "", Collections.emptyList());
            }
        };
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }
}
