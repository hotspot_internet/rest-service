package hotspotinternet.restservice.main;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;

import hotspotinternet.restservice.entity.InterfaceActivity;

// RestTemplate cannot handle generic types, so we need this wrapper class
class ActivityCollectionModel
        extends CollectionModel<EntityModel<InterfaceActivity>> {}
