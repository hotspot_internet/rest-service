package hotspotinternet.restservice.main;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.base.Optional;
import com.google.common.collect.Lists;

import hotspotinternet.restservice.entity.DataLimit;
import hotspotinternet.restservice.entity.DataLimitRepository;
import hotspotinternet.restservice.entity.InterfaceActivity;
import hotspotinternet.restservice.entity.InterfaceReading;
import hotspotinternet.restservice.entity.InterfaceReadingRepository;
import hotspotinternet.restservice.interval.ActivityPeriods;
import hotspotinternet.restservice.interval.ActivityPeriodsFactory;
import hotspotinternet.restservice.interval.ReadingInterval;

/**
 * Controls/handles requests to interface reading URL ('/reading/interface/').
 */
@CrossOrigin
@Controller
public class InterfacesController {
    @Value("${interface.reading.adjustmentmulitiplier}")
    private double adjustmentMulitiplier;

    @Autowired
    private DataLimitRepository dataLimitRepo;
    @Autowired
    private InterfaceReadingRepository interfaceReadingRepo;

    /**
     * Handles POST requests to '/reading/interface/{interfaceIndex}' URL.
     *
     * @return InterfaceReading with written interface reading
     */
    @PostMapping("/reading/interface/{interfaceIndex}")
    public @ResponseBody InterfaceReading postReading(
            @PathVariable Integer interfaceIndex,
            @RequestBody InterfaceReading interfaceReading) {
        return interfaceReadingRepo.save(interfaceReading);
    }

    /**
     * Handles GET requests to '/activity/interface/{interfaceIndex}' URL.
     *
     * @param interfaceIndex for which to get activities
     * @param isDivided that is true if each period(s) should be divided into
     *      shorter-duration periods. For example, divide an hour into 60 minutes
     * @param startParam start of period for which to get activities
     * @param period of time for which to get activities
     * @param activitiesQuantity number of interface activities to return
     * @return CollectionModel of InterfaceActivity instances
     */
    @GetMapping("/activity/interface/{interfaceIndex}")
    public @ResponseBody CollectionModel<EntityModel<InterfaceActivity>> getActivityCollectionModel(
            @PathVariable Integer interfaceIndex,
            @RequestParam(value = "divided", defaultValue = "false") boolean isDivided,
            @RequestParam(value = "start", required = false) Instant startParam,
            @RequestParam(value = "period", defaultValue = "MONTH") ReadingInterval period,
            @RequestParam(value = "quantity", defaultValue = "1") Integer activitiesQuantity) {
        if (activitiesQuantity < 1) {
            throw new RequestException("'quantity' must be positive integer");
        }

        final InterfaceReading lastReading = getMostRecentReading(interfaceIndex);
        final Instant startInstant = startParam == null
                ? lastReading.getInstant()
                : startParam;

        final DataLimit dataLimit = getEarliestDataLimit(interfaceIndex, startInstant);
        Function<Instant, ActivityPeriods> periodsFunction =
                getPeriodsFunction(startInstant, dataLimit, period);

        List<ActivityPeriods> periodsList = new ArrayList<>();
        OffsetDateTime startTime = startInstant.atOffset(ZoneOffset.UTC);

        List<ActivityPeriods> topLevelPeriodsList = IntStream
                .range(0, activitiesQuantity)
                .boxed()
                .map((periodIndex) -> startTime
                        .plus(periodIndex, period.getChronoUnit())
                        .toInstant())
                .map(periodsFunction)
                .collect(Collectors.toList());

        for (ActivityPeriods activityPeriods : topLevelPeriodsList) {
            if (isDivided && !activityPeriods.getDownPeriods().isPresent()) {
                throw new RequestException(String.format(
                        "Period '%s' cannnot be divided into smaller periods",
                        period));
            }

            periodsList.addAll(isDivided
                    ? activityPeriods.getDownPeriods().get()
                    : Collections.singletonList(activityPeriods));
        }

        ReadingInterval modelItemPeriodDuration = getPeriodsDuration(periodsList);

        List<Instant> readingInstants = getInstants(
                startInstant,
                modelItemPeriodDuration,
                periodsList.size() + 1);
        List<InterfaceReading> allReadingsInRange = getReadings(
                interfaceIndex,
                readingInstants.get(0),
                readingInstants.get(readingInstants.size() - 1),
                modelItemPeriodDuration);
        if (allReadingsInRange.isEmpty()) {
            throw new RequestException("Insufficient readings for requested period");
        }

        final List<InterfaceReading> cumulativeReadings = getCumulativeReadings(allReadingsInRange);
        final List<InterfaceReading> readings = getReadingsAtInstants(
                readingInstants,
                cumulativeReadings);

        Duration durationSinceLastReading = Duration.between(
                lastReading.getInstant(),
                Instant.now());
        boolean isLastReadingRecent = durationSinceLastReading.abs().toMinutes() < 10;
        Instant lastReadingInstant = lastReading.getInstant();
        boolean isLastReadingNeeded = readingInstants
                .stream()
                .anyMatch(lastReadingInstant::isBefore);
        if (isLastReadingNeeded && isLastReadingRecent) {
            InterfaceReading lastCumulativeReading =
                    cumulativeReadings.get(cumulativeReadings.size() - 1);
            readings.add(lastCumulativeReading);
        }

        if (readings.size() < 2) {
            throw new RequestException("Insufficient readings for requested period");
        }
        final List<InterfaceActivity> activities = InterfaceActivity.fromReadings(readings);

        if (periodsList.size() < activities.size()) {
            throw new IllegalStateException(
                    "Periods List size must not be less than activities list size");
        }

        final InterfaceActivityLinkFactory linkFactory = new InterfaceActivityLinkFactory(
                interfaceIndex,
                topLevelPeriodsList,
                isDivided);
        final List<EntityModel<InterfaceActivity>> activityModels =
                Lists.newArrayListWithExpectedSize(activities.size());
        for (int acitivityIndex = 0; acitivityIndex < activities.size(); ++acitivityIndex) {
            final InterfaceActivity activity = activities.get(acitivityIndex);
            final ActivityPeriods activityPeriods = periodsList
                    .stream()
                    .filter(nextActivityPeriods ->
                            activity.getStartInstant().equals(nextActivityPeriods.getStart()))
                    .findAny()
                    .get();

            List<Link> links = new ArrayList<>();
            links.add(linkFactory.activityPreviousLink(activityPeriods.getPreviousPeriods()));
            links.add(linkFactory.activitySelfLink(activity, modelItemPeriodDuration));
            links.add(linkFactory.activityNextLink(activityPeriods.getNextPeriods()));
            linkFactory.activityIntervalInLink(activityPeriods)
                    .toJavaUtil()
                    .ifPresent(links::add);
            linkFactory.activityIntervalContainsLink(activityPeriods)
                    .toJavaUtil()
                    .ifPresent(links::add);

            EntityModel<InterfaceActivity> model = EntityModel.of(
                    activity,
                    links.toArray(new Link[links.size()]));
            activityModels.add(model);
        }

        List<Link> links = new ArrayList<>();
        links.add(linkFactory.collectionPreviousLink());
        links.add(linkFactory.collectionSelfLink());
        links.add(linkFactory.collectionNextLink());
        linkFactory.collectionIntervalInLink()
                .toJavaUtil()
                .ifPresent(links::add);
        linkFactory.collectionIntervalContainsLink()
                .toJavaUtil()
                .ifPresent(links::add);
        return CollectionModel.of(
                activityModels,
                links.toArray(new Link[links.size()]));
    }

    private List<InterfaceReading> getReadings(final int interfaceIndex,
            final Instant startInstant,
            final Instant stopInstant,
            final ReadingInterval padding) {
        final Function<Long, Long> adjuster = valueLong ->
                BigDecimal.valueOf(valueLong)
                        .multiply(BigDecimal.valueOf(adjustmentMulitiplier))
                        .longValue();

        return interfaceReadingRepo
                .findByInterfaceIndexAndInstantBetween(
                        interfaceIndex,
                        // get readings before and after desired range, so that we have
                        // sufficient values to interpolate at all points
                        subtract(startInstant, 1, padding.getChronoUnit()),
                        add(stopInstant, 1, padding.getChronoUnit()))
                .stream()
                .sorted(Comparator.comparing(InterfaceReading::getInstant))
                .map(interfaceReading -> interfaceReading.adjustBytes(adjuster))
                .collect(Collectors.toList());
    }

    private static ReadingInterval getPeriodsDuration(final List<ActivityPeriods> periodsList) {
        Set<ReadingInterval> readingIntervals = periodsList
                .stream()
                .map(ActivityPeriods::getDuration)
                .collect(Collectors.toSet());
        if (readingIntervals.size() != 1) {
            throw new IllegalStateException("Expected all down periods to have same duration");
        }
        return readingIntervals.iterator().next();
    }

    private static Function<Instant, ActivityPeriods> getPeriodsFunction(
            final Instant startInstant,
            final DataLimit dataLimit,
            final ReadingInterval period) {
        ActivityPeriodsFactory periodsFactory = new ActivityPeriodsFactory(dataLimit);
        switch (period) {
            case MINUTE:
                return periodsFactory::minutePeriods;
            case HOUR:
                return periodsFactory::hourPeriods;
            case DAY:
                return periodsFactory::dayPeriods;
            case MONTH:
                return periodsFactory::cyclePeriods;
            case YEAR:
                return periodsFactory::yearPeriods;
            default:
                throw new IllegalArgumentException("Unexpected value: " + period);
        }
    }

    /**
     * Handles GET requests to '/reading/interface/{interfaceIndex}' URL.
     *
     * @param interfaceIndex for which to get readings
     * @param startParam start of period for which to get readings
     * @param readingInterval duration between between readings in returned list
     * @param readingsQuantity number of readings to return
     * @return CollectionModel of InterfaceReadings with cumulative values
     */
    @GetMapping("/reading/interface/{interfaceIndex}")
    public @ResponseBody CollectionModel<EntityModel<InterfaceReading>> getReadingsCollectionModel(
            @PathVariable Integer interfaceIndex,
            @RequestParam(value = "start", required = false) Instant startParam,
            @RequestParam(value = "interval", defaultValue = "DAY") ReadingInterval readingInterval,
            @RequestParam(value = "quantity", defaultValue = "1") Integer readingsQuantity) {
        if (readingsQuantity < 1) {
            throw new RequestException("'quantity' must be positive integer");
        }

        final Instant startInstant = startParam == null
                ? getMostRecentReading(interfaceIndex).getInstant()
                : startParam;

        DataLimit dataLimit = getEarliestDataLimit(interfaceIndex, startInstant);
        List<Instant> readingInstants =
                getInstants(startInstant, readingInterval, readingsQuantity);
        List<InterfaceReading> allReadingsInRange = getReadings(
                interfaceIndex,
                dataLimit.getStartInstant(startInstant).get(),
                readingInstants.get(readingInstants.size() - 1),
                readingInterval);
        if (allReadingsInRange.isEmpty()) {
            throw new RequestException("No readings available for provided index and parameters");
        }

        final List<InterfaceReading> cumulativeReadings =
                getCumulativeReadings(allReadingsInRange);
        final List<InterfaceReading> cycleCumulativeReadings =
                getCycleCumulativeReadings(cumulativeReadings, dataLimit);
        final List<InterfaceReading> readingsAtInstants =
                getReadingsAtInstants(readingInstants, cycleCumulativeReadings);

        final List<EntityModel<InterfaceReading>> readingModels = readingsAtInstants
                .stream()
                .map(reading -> EntityModel.of(
                        reading,
                        createReadingLink(
                                reading.getInterfaceIndex(),
                                reading.getInstant(),
                                null,
                                1)))
                .collect(Collectors.toList());
        return CollectionModel.of(
                readingModels,
                createReadingLink(interfaceIndex, startInstant, readingInterval, readingsQuantity));
    }

    private static Link createReadingLink(
            final Integer interfaceIndex,
            final Instant startInstant,
            final ReadingInterval readingInterval,
            final Integer readingsQuantity) {
        return linkTo(methodOn(InterfacesController.class).getReadingsCollectionModel(
                interfaceIndex,
                startInstant,
                readingInterval,
                readingsQuantity)).withSelfRel();
    }

    private static Instant add(final Instant instant,
            final int quantity,
            final TemporalUnit unit) {
        return instant
                .atOffset(ZoneOffset.UTC)
                .plus(quantity, unit)
                .toInstant();
    }

    private static Instant subtract(final Instant instant,
            final int quantity,
            final TemporalUnit unit) {
        return instant
                .atOffset(ZoneOffset.UTC)
                .minus(quantity, unit)
                .toInstant();
    }

    private static List<Instant> getInstants(
            final Instant startInstant,
            final ReadingInterval readingInterval,
            final Integer readingsQuantity) {
        return IntStream.range(0, readingsQuantity)
                .boxed()
                .map(index -> add(startInstant, index, readingInterval.getChronoUnit()))
                .collect(Collectors.toList());
    }

    private static List<InterfaceReading> getCycleCumulativeReadings(
            final List<InterfaceReading> cumulativeReadings,
            final DataLimit dataLimit) {
        InterfaceReading last = cumulativeReadings.get(0);
        InterfaceReading total = last.zero();
        Instant dataLimitStop = dataLimit.getStopInstant(last.getInstant()).get();

        List<InterfaceReading> cycleCumulativeReadings =
                Lists.newArrayListWithExpectedSize(cumulativeReadings.size());
        for (InterfaceReading interfaceReading : cumulativeReadings) {
            if (!dataLimitStop.isAfter(interfaceReading.getInstant())) {
                InterfaceReading readingBeforeStop = InterfaceReading.getEstimatedReading(
                        last,
                        interfaceReading,
                        dataLimitStop.minusNanos(1));
                InterfaceReading readingAtStop = InterfaceReading.getEstimatedReading(
                        last,
                        interfaceReading,
                        dataLimitStop);
                cycleCumulativeReadings.add(readingBeforeStop.substract(last).add(total));
                total = readingAtStop.zero();
                cycleCumulativeReadings.add(total);
                total = interfaceReading.substract(readingAtStop).add(total);

                Optional<Instant> dataLimitNextStop =
                        dataLimit.getStopInstant(interfaceReading.getInstant());
                if (dataLimitNextStop.isPresent()) {
                    dataLimitStop = dataLimitNextStop.get();
                } else {
                    // remaining readings are beyond stop instant of data limit
                    break;
                }
            } else {
                total = interfaceReading.substract(last).add(total);
            }
            last = interfaceReading;
            cycleCumulativeReadings.add(total);
        }
        return cycleCumulativeReadings;
    }

    private static List<InterfaceReading> getReadingsAtInstants(
            final List<Instant> readingInstants,
            final List<InterfaceReading> readings) {
        List<InterfaceReading> readingsAtInstants =
                Lists.newArrayListWithExpectedSize(readingInstants.size());

        int readingIndex = 0;
        for (Instant readingInstant : readingInstants) {
            while (readingIndex < readings.size()) {
                InterfaceReading currentReading = readings.get(readingIndex);
                // set to null since resultant reading does not correspond to a raw
                // that has an ID
                currentReading.setMeasurementId(null);

                if (currentReading.getInstant().equals(readingInstant)) {
                    readingsAtInstants.add(currentReading);
                    break;
                } else if (readingIndex == readings.size() - 1) {
                    ++readingIndex;
                    // there is no next reading to be computed below
                    break;
                }

                InterfaceReading nextReading = readings.get(readingIndex + 1);
                if (currentReading.getInstant().isBefore(readingInstant)
                        && nextReading.getInstant().isAfter(readingInstant)) {
                    readingsAtInstants.add(
                            InterfaceReading.getEstimatedReading(
                                    currentReading,
                                    nextReading,
                                    readingInstant));
                    break;
                } else if (currentReading.getInstant().isAfter(readingInstant)) {
                    break;
                }
                ++readingIndex;
            }
        }
        return readingsAtInstants;
    }

    private static List<InterfaceReading> getCumulativeReadings(
            final List<InterfaceReading> readings) {
        List<InterfaceReading> cumulativeReadings =
                Lists.newArrayListWithExpectedSize(readings.size());
        InterfaceReading last = readings.get(0);
        InterfaceReading total = last.zero();

        for (InterfaceReading interfaceReading : readings) {
            if (interfaceReading.getInBytes() < last.getInBytes()) {
                total = interfaceReading.add(total);
            } else {
                total = interfaceReading.substract(last).add(total);
            }
            last = interfaceReading;
            cumulativeReadings.add(total);
        }
        return cumulativeReadings;
    }

    private InterfaceReading getMostRecentReading(final int interfaceIndex) {
        return interfaceReadingRepo
                .findFirstByInterfaceIndexOrderByInstantDesc(interfaceIndex)
                .orElseThrow(() ->
                        new RequestException("No readings for interface: " + interfaceIndex));
    }

    private DataLimit getEarliestDataLimit(final int interfaceIndex, final Instant instant) {
        return dataLimitRepo
                .findByInterfaceIndex(interfaceIndex)
                .stream()
                .filter(dataLimit -> dataLimit.getStartInstant(instant).isPresent())
                .sorted(Comparator.comparing(
                        dataLimit -> dataLimit.getStartInstant(instant).get()))
                .findFirst()
                .orElseThrow(() ->
                        new RequestException("No data limit relevant to instant: " + instant));
    }
}

