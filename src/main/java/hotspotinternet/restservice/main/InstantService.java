package hotspotinternet.restservice.main;

import java.time.Instant;

import org.springframework.stereotype.Service;

/**
 * Calculates 'now' instant.
 */
@Service
public class InstantService {

    public Instant now() {
        return Instant.now();
    }
}
