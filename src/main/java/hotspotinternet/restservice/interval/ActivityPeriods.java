package hotspotinternet.restservice.interval;

import java.time.Instant;
import java.util.List;

import com.google.common.base.Optional;

/**
 * A series of chronologically adjacent activity periods. Has information
 * about the duration of each period and how many periods are present.
 */
public interface ActivityPeriods {

    /**
     * Gets start instant for the series of periods.
     *
     * @return start instant
     */
    Instant getStart();

    /**
     * The total duration represented by this class. If down periods are
     * present, then it is equivalent to the sum of those periods.
     *
     * @return total duration represented by this class
     */
    ReadingInterval getDuration();

    /**
     * Gets series of periods that encompass this series of
     * activity periods or absent if there is no encompassing activity periods.
     *
     * @return series of periods that encompass this series or absent if none
     */
    Optional<ActivityPeriods> getUpPeriods();

    /**
     * Gets information about how each period of this series can be divided
     * into a series of shorter periods, or absent if no shorter period
     * available.
     *
     * @return a list of activity periods, where each item is a division of
     *      each period of this series into a series of shorter-duration periods
     */
    Optional<List<ActivityPeriods>> getDownPeriods();

    /**
     * Gets activity periods of same duration that ends at same instant that
     * this series of periods begins.
     *
     * @return activity periods preceding this series of activity periods
     */
    ActivityPeriods getPreviousPeriods();

    /**
     * Gets activity periods of same duration immediately following end of this
     * series of activity periods.
     *
     * @return activity periods following end of this series of activity periods
     */
    ActivityPeriods getNextPeriods();
}
