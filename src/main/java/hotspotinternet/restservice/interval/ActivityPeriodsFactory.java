package hotspotinternet.restservice.interval;

import static com.google.common.base.Preconditions.checkArgument;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.common.base.Optional;

import hotspotinternet.restservice.entity.DataLimit;
import hotspotinternet.restservice.entity.DataLimit.CyclePeriod;

/**
 * Factory class for creating instances of ActivityPeriods.
 */
public class ActivityPeriodsFactory {
    private final DataLimit dataLimit;

    /**
     * Constructs factory from provided data limit.
     *
     * @param dataLimit applicable to the instants that will be used
     *      to create instances of ActivityPeriods
     */
    public ActivityPeriodsFactory(final DataLimit dataLimit) {
        checkArgument(
                dataLimit.getCyclePeriod() == CyclePeriod.MONTH,
                "Only monthly recurring data limits currently supported");
        this.dataLimit = dataLimit;
    }

    private abstract class ActivityPeriodsBase implements ActivityPeriods {

        private final Instant startInstant;
        private final ReadingInterval duration;
        private final Function<Instant, ActivityPeriods> periodsFunction;
        private final ReadingInterval periodUnit;
        private final Integer periodsQuantity;
        private final Function<Instant, ActivityPeriods> downPeriodsFunction;

        protected ActivityPeriodsBase(
                final Instant startInstant,
                final ReadingInterval duration,
                final Function<Instant, ActivityPeriods> periodsFunction,
                final Integer periodsQuantity,
                final ReadingInterval periodUnit,
                final Function<Instant, ActivityPeriods> downPeriodsFunction) {
            this.duration = duration;
            this.periodsFunction = periodsFunction;
            this.periodsQuantity = periodsQuantity;
            this.periodUnit = periodUnit;
            this.startInstant = startInstant;
            this.downPeriodsFunction = downPeriodsFunction;
        }

        @Override
        public Instant getStart() {
            return startInstant;
        }

        @Override
        public ReadingInterval getDuration() {
            return duration;
        }

        protected Integer getPeriodsQuantity() {
            return periodsQuantity;
        }

        @Override
        public Optional<List<ActivityPeriods>> getDownPeriods() {
            return downPeriodsFunction == null
                    ? Optional.absent()
                    : Optional.of(IntStream.range(0, getPeriodsQuantity())
                            .boxed()
                            .map(index -> utcZoneTime(getStart())
                                    .plus(index, periodUnit.getChronoUnit())
                                    .toInstant())
                            .map(downPeriodsFunction)
                            .collect(Collectors.toList()));
        }

        @Override
        public ActivityPeriods getPreviousPeriods() {
            return periodsFunction.apply(utcZoneTime(getStart())
                    .minus(1, getDuration().getChronoUnit())
                    .toInstant());
        }

        @Override
        public ActivityPeriods getNextPeriods() {
            return periodsFunction.apply(utcZoneTime(getStart())
                    .plus(1, getDuration().getChronoUnit())
                    .toInstant());
        }

        @Override
        public boolean equals(Object other) {
            if (!(other instanceof ActivityPeriodsBase)) {
                return false;
            }
            ActivityPeriodsBase otherActivityPeriods = (ActivityPeriodsBase) other;
            return Objects.equal(getStart(), otherActivityPeriods.getStart())
                    && Objects.equal(getDuration(), otherActivityPeriods.getDuration())
                    && Objects.equal(
                            getPeriodsQuantity(),
                            otherActivityPeriods.getPeriodsQuantity())
                    && Objects.equal(getUpPeriods(), otherActivityPeriods.getUpPeriods());
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(
                    getStart(),
                    getDuration(),
                    getPeriodsQuantity(),
                    getUpPeriods());
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("Start", getStart())
                    .add("Duration", getDuration())
                    .add("Periods Quantity", getPeriodsQuantity())
                    .add("Up Periods", getUpPeriods())
                    .toString();
        }
    }

    /**
     * Creates ActivityPeriods that divide an hour into a series of shorter periods.
     *
     * @param startInstant start of series of periods
     * @return ActivityPeriods that divide an hour into a series of shorter periods
     */
    public ActivityPeriods minutePeriods(final Instant startInstant) {
        return new ActivityPeriodsBase(
                startInstant,
                ReadingInterval.MINUTE,
                this::minutePeriods,
                null,
                null,
                null) {
            @Override
            public Optional<ActivityPeriods> getUpPeriods() {
                Instant hourPeriodsStart = utcZoneTime(startInstant)
                        .withSecond(0)
                        .toInstant();
                return Optional.of(hourPeriods(hourPeriodsStart));
            }
        };
    }

    /**
     * Creates ActivityPeriods that divide an hour into a series of shorter periods.
     *
     * @param startInstant start of series of periods
     * @return ActivityPeriods that divide an hour into a series of shorter periods
     */
    public ActivityPeriods hourPeriods(final Instant startInstant) {
        return new ActivityPeriodsBase(
                startInstant,
                ReadingInterval.HOUR,
                this::hourPeriods,
                60,
                ReadingInterval.MINUTE,
                this::minutePeriods) {
            @Override
            public Optional<ActivityPeriods> getUpPeriods() {
                OffsetDateTime startDateTime = utcZoneTime(startInstant);
                Optional<Instant> dataCycleStart = dataLimit.getStartInstant(startInstant);
                if (!dataCycleStart.isPresent()) {
                    return Optional.absent();
                }

                OffsetDateTime cycleDayStartForSameDay = utcZoneTime(dataCycleStart.get())
                        .withDayOfYear(startDateTime.getDayOfYear())
                        .withYear(startDateTime.getYear());
                boolean usePreviousDay = !startDateTime.isAfter(cycleDayStartForSameDay);
                OffsetDateTime dayPeriodsStart = usePreviousDay
                        ? cycleDayStartForSameDay.minus(1, ChronoUnit.DAYS)
                        : cycleDayStartForSameDay;
                return Optional.of(dayPeriods(dayPeriodsStart.toInstant()));
            }
        };
    }

    /**
     * Creates ActivityPeriods that divide a day into a series of shorter periods.
     *
     * @param startInstant start of series of periods
     * @return ActivityPeriods that divide a day into a series of shorter periods
     */
    public ActivityPeriods dayPeriods(final Instant startInstant) {
        return new ActivityPeriodsBase(
                startInstant,
                ReadingInterval.DAY,
                this::dayPeriods,
                24,
                ReadingInterval.HOUR,
                this::hourPeriods) {
            @Override
            public Optional<ActivityPeriods> getUpPeriods() {
                OffsetDateTime dataLimitStartTime = utcZoneTime(dataLimit.getStartInstant());
                OffsetDateTime startTime = utcZoneTime(startInstant);
                OffsetDateTime sameMonthDataLimitStart = dataLimitStartTime
                        .withMonth(startTime.getMonthValue())
                        .withYear(startTime.getYear());

                boolean isSameMonthLimitAfterStart = sameMonthDataLimitStart
                        .toInstant()
                        .isAfter(startTime.toInstant());
                OffsetDateTime cyclePeriodsStartTime = isSameMonthLimitAfterStart
                        ? sameMonthDataLimitStart.minusMonths(1)
                        : sameMonthDataLimitStart;
                return Optional.of(cyclePeriods(cyclePeriodsStartTime.toInstant()));
            }
        };
    }

    /**
     * Creates ActivityPeriods that divide a month cycle into a series of shorter periods.
     *
     * @param startInstant start of series of periods
     * @return ActivityPeriods that divide month cycle into a series of shorter periods
     */
    public ActivityPeriods cyclePeriods(final Instant startInstant) {
        return new ActivityPeriodsBase(
                startInstant,
                ReadingInterval.MONTH,
                this::cyclePeriods,
                null,
                ReadingInterval.DAY,
                this::dayPeriods) {
            @Override
            public Optional<ActivityPeriods> getUpPeriods() {
                OffsetDateTime dataLimitStartTime = utcZoneTime(dataLimit.getStartInstant());
                OffsetDateTime startTime = utcZoneTime(startInstant);
                OffsetDateTime januaryDataLimitStart = dataLimitStartTime
                        .withMonth(1)
                        .withYear(startTime.getYear());

                boolean isJanuaryLimitAfterStart = januaryDataLimitStart
                        .toInstant()
                        .isAfter(startTime.toInstant());
                OffsetDateTime yearPeriodsStartTime = isJanuaryLimitAfterStart
                        ? januaryDataLimitStart.minusYears(1)
                        : januaryDataLimitStart;
                return Optional.of(yearPeriods(yearPeriodsStartTime.toInstant()));
            }

            @Override
            public Integer getPeriodsQuantity() {
                return (int) ChronoUnit.DAYS.between(
                        startInstant,
                        utcZoneTime(startInstant).plusMonths(1).toInstant());
            }
        };
    }

    /**
     * Creates ActivityPeriods that divide a year into a series of shorter periods.
     *
     * @param startInstant start of series of periods
     * @return ActivityPeriods that divide a year into a series of shorter periods
     */
    public ActivityPeriods yearPeriods(final Instant startInstant) {
        return new ActivityPeriodsBase(
                startInstant,
                ReadingInterval.YEAR,
                this::yearPeriods,
                12,
                ReadingInterval.MONTH,
                this::cyclePeriods) {
            @Override
            public Optional<ActivityPeriods> getUpPeriods() {
                return Optional.absent();
            }
        };
    }

    private static OffsetDateTime utcZoneTime(final Instant instant) {
        return instant.atOffset(ZoneOffset.UTC);
    }
}
