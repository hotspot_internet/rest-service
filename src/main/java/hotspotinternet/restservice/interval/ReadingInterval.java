package hotspotinternet.restservice.interval;

import java.time.temporal.ChronoUnit;

/**
 * An interval of time.
 */
public enum ReadingInterval {
    MINUTE(ChronoUnit.MINUTES),
    HOUR(ChronoUnit.HOURS),
    DAY(ChronoUnit.DAYS),
    MONTH(ChronoUnit.MONTHS),
    YEAR(ChronoUnit.YEARS);

    private final ChronoUnit chronoUnit;

    private ReadingInterval(final ChronoUnit chronoUnit) {
        this.chronoUnit = chronoUnit;
    }

    /**
     * Gets chrono unit of interval.
     *
     * @return unit of interval
     */
    public ChronoUnit getChronoUnit() {
        return chronoUnit;
    }
}
